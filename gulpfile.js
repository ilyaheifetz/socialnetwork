var gulp = require('gulp');
var concatCss = require('gulp-concat-css');
var autoprefixer = require('gulp-autoprefixer');

 
gulp.task('default', function () {
  gulp.src('app/webroot/css/main.css')
    .pipe(concatCss("bundle.css"))
    .pipe(autoprefixer({
            browsers: ['last 7 versions'],
            cascade: false
    	}))
    .pipe(gulp.dest('app/webroot/css/'));
});

/*
gulp.task('default', function () {
    return gulp.src('app/webroot/css/bundle.css')
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
    .pipe(gulp.dest('app/webroot/css/'));
});
*/