<?php
App::uses('LikesController', 'Controller');

/**
 * LikesController Test Case
 *
 */
class LikesControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.like'
	);

}
