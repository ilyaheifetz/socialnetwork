$(document).ready(function () {
    /*
    $(document).ajaxStart(function() {
        var subscribeButtonWidth = $('#subscribeButton').width();
        $('#subscribeButton').width(subscribeButtonWidth);
        $('#subscribeButton').attr('disabled', 'disabled');
        $('#subscribeButton').html('<img src="/app/webroot/img/ajax-loader-fb.gif" alt="" />');
    });

    $( document ).ajaxStop(function() {
        $('#subscribeButton').removeAttr('disabled');
        $('#subscribeButton').css('width', 'auto');
    });
*/
    $('a#subscribeButton').click(function() {
        console.log('click');
        var profile_author_id = $(this).data('profileAuthorId');
        if($(this).text() == 'Subscribe') {
            var targeturl = '/profiles/subscribe/' + profile_author_id;
        } else {
            var targeturl = '/profiles/unsubscribe/' + profile_author_id;
        };
        var thisButton = $(this);
        var thisButtonBackupHtml = thisButton.html();

        // ajaxStart
        ajaxStart(thisButton, 'ajax-loader-fb.gif');

        $.ajax({
            type: 'get',
            url: targeturl,
            dataType: 'json',
            success: function(data){
                // ajaxStop
                ajaxStop(thisButton);               

                if(data.status) {
                console.log(data);
                    thisButton.text(data.button_title);
                    console.log(data.message);

                    // Set subscribers count
                    if ($('.subscribers-and-subscriptions-count').length) {
                        if((!data.subscribers_count)) {
                            $('.subscribers-and-subscriptions-count__subscribers-title').css("display", "none");
                        } else {
                            $('.subscribers-and-subscriptions-count__subscribers-count').text(data.subscribers_count);
                            $('.subscribers-and-subscriptions-count__subscribers-title').css("display", "inline");
                        }
                    }
                }
            },
            error: function(xhr,textStatus,error){
                ajaxStop(thisButton);
                
                console.log(textStatus);
            }
        });
        return false;
    });
});