$('.like').on('click',function() {

    var id = $(this).data('id');
    var $this = $(this);

    $(this).css('opacity', function(i,o){
        return parseFloat(o).toFixed(1) === '0.6' ? 1 : 0.6;
    });

    $.ajax({
        url: '/profiles/like/' + id,
        dataType: "json",
        success: function(responce) {
            $this.find('.likes-count').text(responce);
        }
    })
});