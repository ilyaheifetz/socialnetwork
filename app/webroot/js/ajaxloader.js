function ajaxStart(obj, ajaxLoaderImgName){
    var objWidth = obj.width();
    var objHeight = obj.height();
    obj.width(objWidth);
    obj.attr('disabled', 'disabled');
    obj.html('<img src="/app/webroot/img/' + ajaxLoaderImgName + '" alt="" />');
}

function ajaxStop(obj){
    obj.removeAttr('disabled');
    obj.css('width', 'auto');
    obj.blur();
}

function ajaxStopError(obj, objBackupHtml){
	obj.removeAttr('disabled');
	obj.html(objBackupHtml);
	obj.blur();
}