<?php
	// foreach ($likingPost as $value) {
	// 	echo $value['Post']['body'];
	// 	echo $value['Profile']['firstname'];
	// 	echo $value['Profile']['lastname'];
	// }
?>
<div class="container">
	<div class="row row-offcanvas row-offcanvas-right">
	    <div class="col-xs-8">
	      <p class="pull-right visible-xs">
	        <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button>
	      </p>
	      <div class="row">
	      	<?php foreach ($likingPost as $value) { ?>
	        <div class="col-4 col-sm-4">
	          <h2><?php echo $value['Post']['title'];?></h2>
	          <p><?php echo $value['Post']['body']; ?></p>
	          <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
	        </div><!--/span-->
	        <?php } ?>
	      </div><!--/row-->
	    </div><!--/span-->
	</div><!--/row-->
</div><!--/.container-->