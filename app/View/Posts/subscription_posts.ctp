<?php 
    echo $this->element('posts', array(
        'stylesExternal' => 'posts_supscription-posts',
        'stylesInner' => 'col-md-10 col-md-offset-1',
        'posts' => $posts,
        'postTitle' => 'Subscription Post',
        'userId' => $userId,
        'profileId' => $profileId,
        'allLikesCurrentProfile' => $allLikesCurrentProfiles
    ));