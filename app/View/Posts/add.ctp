<?php 
	$formDesignOptions = array(
		'inputDefaults' => array(
			'div' => 'form-group',
			'class' => 'form-control'
		),
		'class' => 'form-horizontal col-lg-8 col-centered'
	);
	$formSubmitOptions = array(
		'div' => 'col',
		'class' => 'btn btn-primary'
	);
?>
<h1 class="profile__title"><?php echo __('Add a New Post'); ?></h1>
<hr>
<div class="row">
	<div class="col-md-3"></div>
	<div class="col-md-9">
		<?php
			echo $this->Form->create('Post', $formDesignOptions);			
			echo $this->Form->input('title');
			echo $this->Form->input('body');
		?>
		<div class="form-group">
			<?php echo $this->Form->submit(__('Submit'), $formSubmitOptions); ?>
		</div>
	</div>
</div>

