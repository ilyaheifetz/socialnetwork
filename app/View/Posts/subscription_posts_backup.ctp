
<?php
/*
	foreach ($subPost as $value) {
		pr($value);
	}*/
	//pr($subPost);
    //pr($subscriptionPosts);
?>

<?php pr($posts); ?>
<div class="posts">
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12 posts__header-count"> 
                    <h3>
                        <?php echo count($posts) . ((count($posts) == 1) ? ' Post' : ' Posts'); ?> 
                    </h3>
                </div>
            </div>
            <?php foreach($posts as $post): ?>
                <?php if(!$post['Post']['parent_id']): ?>
                    <div class="posts__post row" id="<?php echo 'post_id=' . $post['Post']['id']; ?>">
                        <?php //pr($post); ?>
                        <?php 
                            if(isset($post['Profile']['pic_dir'])) {
                                $pic_dir = $post['Profile']['pic_dir'];
                                $image = $this->Html->image($pic_dir, array(
                                    'alt' => 'Photo',
                                    'class' => 'img-rounded post__user-avatar-image'
                                ));
                            } else {
                                $image = $this->Html->image("//placehold.it/100", array(
                                    'alt' => 'Photo',
                                    'class' => 'img-rounded post__user-avatar-image'
                                ));
                            }                
                        ?>
                        <div class="col-md-2 post__user-avatar-wrapper">
                            <?php  
                                echo $this->Html->link(
                                    $image,
                                    array(
                                        'controller' => 'profiles',
                                        'action' => 'view',
                                        $post['Profile']['id'],
                                        'comments' => false
                                    ),
                                    array('escape' => false)
                                );
                            ?>
                        </div>
                        <?php
                            $name = '';
                            $link = '';
                            if ($post['Profile']['firstname']) {
                                $name = $post['Profile']['firstname'];
                            }
                            if ($post['Profile']['lastname']) {
                                if ($post['Profile']['firstname']) {
                                    $name .= ' ';
                                }
                                $name .= $post['Profile']['lastname'];                        
                            }
                            if($name) {  
                                $link = $this->Html->link(
                                    h($name),
                                    array(
                                        'controller' => 'profiles',
                                        'action' => 'view',
                                        $post['Profile']['id'],
                                        'comments' => false
                                    ),
                                    array('escape' => false)
                                );
                            } 
                        ?>
                        <div class="col-md-10">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <?php if($name): ?>
                                                <?php echo $link; ?>
                                            <?php endif ?>
                                            <span class="text-muted posts__post-created">
                                                <?php
                                                    if($post['Post']['created'] == $post['Post']['modified']) {
                                                        echo h($post['Post']['created']);
                                                    } else {
                                                        echo 'updated ' . h($post['Post']['modified']);
                                                    }                                
                                                ?>
                                            </span>
                                            <?php
                                            if (!empty($userId))
                                                {
                                                if (in_array($post['Post']['id'], $allLikesCurrentProfile)) {
                                            ?>
                                            <span class="like" data-id="<?php echo $post['Post']['id'] ?>">
                                                <i class="fa fa-thumbs-o-up fa-2x"></i>
                                                <span class="likes-count">
                                                    <?php echo count($post['Like'])?count($post['Like']):''; ?>            
                                                </span>
                                            </span>
                                            <?php
                                                } else {
                                            ?>
                                            <span class="like _not-active" data-id="<?php echo $post['Post']['id'] ?>">
                                                <i class="fa fa-thumbs-o-up fa-2x"></i>
                                                <span class="likes-count">
                                                    <?php echo count($post['Like'])?count($post['Like']):''; ?>            
                                                </span>
                                            </span>
                                            <?php 
                                                }
                                            }
                                            ?>
                                        </div>
                                        <div class="panel-body">
                                            <h4 class="posts__post-title">
                                                <?php
                                                    echo $link = $this->Html->link(
                                                        h($post['Post']['title']),
                                                        array(
                                                            'controller' => 'posts',
                                                            'action' => 'view',
                                                            $post['Post']['id'],
                                                            'comments' => false
                                                        ),
                                                        array('escape' => false)
                                                    );
                                                ?>
                                            </h4>
                                            <hr class="post__undertitle-hr">
                                            <p class="posts__post-body"><?php echo str_replace("\n", '<br/>', h($post['Post']['body'])); ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php 
                                if($post['Comment']) {                               
                                    echo $this->element('comment-posts', array(
                                        'commentPosts' => $post['Comment'], 
                                        'allLikesCurrentProfile' => $allLikesCurrentProfile
                                    ));
                                }
                            ?>   
                        </div>                         
                    </div>
                <?php endif ?>
            <?php endforeach ?>
        </div>
    </div>
</div>

<script type="text/javascript">
    
    $('.like').on('click',function() {

    var id = $(this).data('id');
    var $this = $(this);

    $(this).css('opacity', function(i,o){
        return parseFloat(o).toFixed(1) === '0.6' ? 1 : 0.6;
    });

    $.ajax({
        url: '/profiles/like/' + id,
        dataType: "json",
        success: function(responce) {
            $this.find('.likes-count').text(responce);
        }
    })
});
</script>



<!--div class="posts posts_supscription-posts">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <?php if(empty($posts)) { ?>
                <div class="row posts__header_underline">
                    <div class="col-md-12 posts__header-count"> 
                        <h3>There are no subscription Posts!</h3>
                    </div>
                </div>
            <?php } else { ?>
                <div class="row">
                    <div class="col-md-12 posts__header-count"> 
                        <h3>
                            <?php echo count($posts) . ' Subscription' . ((count($posts) == 1) ? ' Post' : ' Posts'); ?> 
                        </h3>
                    </div>
                </div>
            <?php } ?>
            <?php if(!empty($posts)): ?>          
                <?php foreach($posts as $post): ?>
                    <?php if(!$post['Post']['parent_id']): ?>
                        <div class="posts__post row" id="<?php echo 'post_id=' . $post['Post']['id']; ?>">
                            <?php //pr($post); ?>
                            <?php 
                                if(isset($post['Profile']['pic_dir'])) {
                                    $pic_dir = $post['Profile']['pic_dir'];
                                    $image = $this->Html->image($pic_dir, array(
                                        'alt' => 'Photo',
                                        'class' => 'img-rounded post__user-avatar-image'
                                    ));
                                } else {
                                    $image = $this->Html->image("//placehold.it/100", array(
                                        'alt' => 'Photo',
                                        'class' => 'img-rounded post__user-avatar-image'
                                    ));
                                }                
                            ?>
                            <div class="col-md-2 post__user-avatar-wrapper">
                                <?php  
                                    echo $this->Html->link(
                                        $image,
                                        array(
                                            'controller' => 'profiles',
                                            'action' => 'view',
                                            $post['Profile']['id'],
                                            'comments' => false
                                        ),
                                        array('escape' => false)
                                    );
                                ?>
                            </div>
                            <?php
                                $name = '';
                                $link = '';
                                if ($post['Profile']['firstname']) {
                                    $name = $post['Profile']['firstname'];
                                }
                                if ($post['Profile']['lastname']) {
                                    if ($post['Profile']['firstname']) {
                                        $name .= ' ';
                                    }
                                    $name .= $post['Profile']['lastname'];                        
                                }
                                if($name) {  
                                    $link = $this->Html->link(
                                        h($name),
                                        array(
                                            'controller' => 'profiles',
                                            'action' => 'view',
                                            $post['Profile']['id'],
                                            'comments' => false
                                        ),
                                        array('escape' => false)
                                    );
                                } 
                            ?>
                            <div class="col-md-10">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <?php if($name): ?>
                                                    <?php echo $link; ?>
                                                <?php endif ?>
                                                <span class="text-muted posts__post-created">
                                                    <?php
                                                        if($post['Post']['created'] == $post['Post']['modified']) {
                                                            echo h($post['Post']['created']);
                                                        } else {
                                                            echo 'updated ' . h($post['Post']['modified']);
                                                        }                                
                                                    ?>
                                                </span>
                                                <span class="like">    
                                                <?php
                                                    if (!empty($userId)) {
                                                        echo $this->Html->link('<i class="fa fa-thumbs-o-up fa-2x"></i>', array('controller' => 'profiles', 'action' => 'like', $post['Post']['id']), array('escape' => false));
                                                        $likeCount = count($post['Like']);
                                                        if (!empty($likeCount)) {
                                                             echo $likeCount;
                                                        }
                                                    }   
                                                ?>
                                                </span>
                                            </div>
                                            <div class="panel-body">
                                                <h4 class="posts__post-title">
                                                    <?php
                                                        echo $link = $this->Html->link(
                                                            h($post['Post']['title']),
                                                            array(
                                                                'controller' => 'posts',
                                                                'action' => 'view',
                                                                $post['Post']['id'],
                                                                'comments' => false
                                                            ),
                                                            array('escape' => false)
                                                        );
                                                    ?>
                                                </h4>
                                                <hr class="post__undertitle-hr">
                                                <p class="posts__post-body">
                                                    <?php echo str_replace("\n", '<br/>', h($post['Post']['body'])); ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php 
                                    if($post['Comment']) {                               
                                        echo $this->element('comment-posts', array(
                                            'commentPosts' => $post['Comment'], 
                                            'allLikesCurrentProfile' => $allLikesCurrentProfile
                                        ));
                                    }
                                ?>   
                            </div>  
                        </div>
                    <?php //endif ?>
                <?php endforeach ?>
            <?php endif ?>
        </div>
    </div>
</div-->

<!--div class="posts posts_supscription-posts">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <?php if(empty($subPost)) { ?>
                <div class="row posts__header_underline">
                    <div class="col-md-12 posts__header-count"> 
                        <h3>There are no subscription Posts!</h3>
                    </div>
                </div>
            <?php } else { ?>
                <div class="row">
                    <div class="col-md-12 posts__header-count"> 
                        <h3>
                            <?php echo count($subPost) . ' Subscription' . ((count($subPost) == 1) ? ' Post' : ' Posts'); ?> 
                        </h3>
                    </div>
                </div>
            <?php } ?>
            <?php if(!empty($subPost)): ?>          
                <?php foreach($subPost as $postA): ?>
                    <?php $post = $postA[0]; ?>
                    <?php //if(!$post['Post']['parent_id']): ?>
                        <div class="posts__post row" id="<?php echo 'post_id=' . $post['Post']['id']; ?>">
                            <?php //pr($post); ?>
                            <?php 
                                if(isset($post['Profile']['pic_dir'])) {
                                    $pic_dir = $post['Profile']['pic_dir'];
                                    $image = $this->Html->image($pic_dir, array(
                                        'alt' => 'Photo',
                                        'class' => 'img-rounded post__user-avatar-image'
                                    ));
                                } else {
                                    $image = $this->Html->image("//placehold.it/100", array(
                                        'alt' => 'Photo',
                                        'class' => 'img-rounded post__user-avatar-image'
                                    ));
                                }                
                            ?>
                            <div class="col-md-2 post__user-avatar-wrapper">
                                <?php  
                                    echo $this->Html->link(
                                        $image,
                                        array(
                                            'controller' => 'profiles',
                                            'action' => 'view',
                                            $post['Profile']['id'],
                                            'comments' => false
                                        ),
                                        array('escape' => false)
                                    );
                                ?>
                            </div>
                            <?php
                                $name = '';
                                $link = '';
                                if ($post['Profile']['firstname']) {
                                    $name = $post['Profile']['firstname'];
                                }
                                if ($post['Profile']['lastname']) {
                                    if ($post['Profile']['firstname']) {
                                        $name .= ' ';
                                    }
                                    $name .= $post['Profile']['lastname'];                        
                                }
                                if($name) {  
                                    $link = $this->Html->link(
                                        h($name),
                                        array(
                                            'controller' => 'profiles',
                                            'action' => 'view',
                                            $post['Profile']['id'],
                                            'comments' => false
                                        ),
                                        array('escape' => false)
                                    );
                                } 
                            ?>
                            <div class="col-md-10">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <?php if($name): ?>
                                                    <?php echo $link; ?>
                                                <?php endif ?>
                                                <span class="text-muted posts__post-created">
                                                    <?php
                                                        if($post['Post']['created'] == $post['Post']['modified']) {
                                                            echo h($post['Post']['created']);
                                                        } else {
                                                            echo 'updated ' . h($post['Post']['modified']);
                                                        }                                
                                                    ?>
                                                </span>
                                                <span class="like">    
                                                <?php
                                                    if (!empty($userId)) {
                                                        echo $this->Html->link('<i class="fa fa-thumbs-o-up fa-2x"></i>', array('controller' => 'profiles', 'action' => 'like', $post['Post']['id']), array('escape' => false));
                                                        $likeCount = count($post['Like']);
                                                        if (!empty($likeCount)) {
                                                             echo $likeCount;
                                                        }
                                                    }   
                                                ?>
                                                </span>
                                            </div>
                                            <div class="panel-body">
                                                <h4 class="posts__post-title">
                                                    <?php
                                                        echo $link = $this->Html->link(
                                                            h($post['Post']['title']),
                                                            array(
                                                                'controller' => 'posts',
                                                                'action' => 'view',
                                                                $post['Post']['id'],
                                                                'comments' => false
                                                            ),
                                                            array('escape' => false)
                                                        );
                                                    ?>
                                                </h4>
                                                <hr class="post__undertitle-hr">
                                                <p class="posts__post-body">
                                                    <?php echo str_replace("\n", '<br/>', h($post['Post']['body'])); ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php 
                                    if($post['children']) {                               
                                        echo $this->element('comment-posts', array('commentPosts' => $post['children']));
                                    }
                                ?>   
                            </div>  
                        </div>
                    <?php //endif ?>
                <?php endforeach ?>
            <?php endif ?>
        </div>
    </div>
</div-->