<?php //pr($post) ?>
<div class="row posts__single-post">
    <div class="col-md-10 col-md-offset-1" style="">
        <?php 
            if(isset($post['Profile']['pic_dir'])) {
                $pic_dir = $post['Profile']['pic_dir'];
                $image = $this->Html->image($pic_dir, array(
                    'alt' => 'Photo',
                    'class' => 'img-rounded subscription-list__avatar'
                ));
            } else {
                $image = $this->Html->image("//placehold.it/100", array(
                    'alt' => 'Photo',
                    'class' => 'img-rounded subscription-list__avatar'
                ));
            }                
        ?>
        <div class="row">
            <div class="col-md-2 post__user-avatar-wrapper">
                <?php  
                    echo $this->Html->link(
                        $image,
                        array(
                            'controller' => 'profiles',
                            'action' => 'view',
                            $post['Profile']['id'],
                            'comments' => false
                        ),
                        array('escape' => false)
                    );
                ?>
            </div>
            <?php
                $name = '';
                $link = '';
                if ($post['Profile']['firstname']) {
                    $name = $post['Profile']['firstname'];
                }
                if ($post['Profile']['lastname']) {
                    if ($post['Profile']['firstname']) {
                        $name .= ' ';
                    }
                    $name .= $post['Profile']['lastname'];                        
                }
                if($name) {  
                    $link = $this->Html->link(
                        h($name),
                        array(
                            'controller' => 'profiles',
                            'action' => 'view',
                            $post['Profile']['id'],
                            'comments' => false
                        ),
                        array('escape' => false)
                    );
                } 
            ?>
            <div class="col-md-10">
                <div class="panel panel-post panel-default">
                    <div class="panel-heading">
                        <?php if($name): ?>
                            <?php echo $link; ?>
                        <?php endif ?>
                        <span class="text-muted posts__post-created">
                            <?php echo h($post['Post']['created']); ?>
                        </span>
                        <?php 
                            if (!empty($userId)) {
                                if (in_array($post['Post']['id'], $allLikesCurrentProfiles)) {
                        ?>
                            <span class="like" data-id="<?php echo $post['Post']['id'] ?>">
                                <i class="fa fa-thumbs-o-up fa-2x"></i>
                                <span class="likes-count">
                                    <?php echo count($post['Like']) ? count($post['Like']) : ''; ?>            
                                </span>
                            </span>
                            <?php
                                } else {
                            ?>
                                <span class="like _not-active" data-id="<?php echo $post['Post']['id'] ?>">
                                    <i class="fa fa-thumbs-o-up fa-2x"></i>
                                    <span class="likes-count">
                                        <?php echo count($post['Like'])?count($post['Like']):''; ?>            
                                    </span>
                                </span>
                        <?php
                                }
                            }
                        ?>

                        <?php
                            echo $this->element('post-toolbar', array(
                                'post' => $post['Post'],
                                'profileId' => $profileId,
                            ));
                        ?>
  
                    </div>
                    <div class="panel-body">
                        <h4 class="posts__post-title">
                            <?php echo h($post['Post']['title']); ?>
                        </h4>
                        <hr class="post__undertitle-hr">
                        <p class="posts__post-body"><?php echo str_replace("\n", '<br/>', h($post['Post']['body'])); ?></p>
                        <div class="post__under-the-post-body">
                            <div class="posts__post-updated">
                                <?php if($post['Post']['created'] != $post['Post']['modified']): ?>
                                    <div class="posts__post-updated-clock"><i class="fa fa-clock-o"></i></div>
                                    <span class="text-muted posts__edit">Last updated <?php echo h($post['Post']['modified']); ?></span>
                                <?php endif ?>
                            </div>                            
                        </div>            
                    </div>
                </div>
                <?php 
                    if($post['Comment']) {
                        $comments = $post['Comment'];
                        echo $this->element('comments', array(
                            'comments' => $comments,
                            'userId' => $userId,
                            'profileId' => $profileId,
                            'allLikesCurrentProfile' => $allLikesCurrentProfiles
                        ));
                    }
                ?>
            </div>            
        </div>
    </div>   
</div>
<?php
    $formDesignOptions = array(
        'inputDefaults' => array(
            'div' => 'form-group',
            'class' => 'form-control'
        ),
        'class' => 'form-horizontal col-lg-8 col-centered'
    );
    $formSubmitOptions = array(
        'div' => 'col',
        'class' => 'btn btn-primary'
    );
?>

<?php if(AuthComponent::user() && !$post['Post']['parent_id']): ?>
    <div class="comment">
        <h3 class="profile__title"><?php echo __('Add a Comment'); ?></h3>
        <hr>
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-9">
                <?php
                    echo $this->Form->create('Post', $formDesignOptions);
                    //echo $this->Form->input('title', array('type' => 'hidden', 'value' => '1'));
                    echo $this->Form->input('body');
                ?>
                <div class="form-group">
                    <?php echo $this->Form->submit(__('Submit'), $formSubmitOptions); ?>
                </div>
            </div>
        </div>
    </div>
<?php endif ?>