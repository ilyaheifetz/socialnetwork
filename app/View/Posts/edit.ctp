<?php 
	$formDesignOptions = array(
		'inputDefaults' => array(
			'div' => 'form-group',
			'class' => 'form-control'
		),
		'class' => 'form-horizontal col-lg-8 col-centered'
	);
	$formSubmitOptions = array(
		'div' => 'col',
		'class' => 'btn btn-primary'
	);
?>
<h1 class="profile__title"><?php echo __('Edit a Post'); ?></h1>
<hr>
<div class="row">
	<div class="col-md-9 col-md-offset-3">
		<div class="row posts__title-section">
			<div class="col-md-12">
				<strong>Title</strong><br/>
				<span class="posts__title-edit"><?php echo h($post['Post']['title']); ?></span>
			</div>
		</div>
		<?php
			echo $this->Form->create('Post', $formDesignOptions);
			echo $this->Form->input('body');
		?>
		<div class="form-group">
			<?php echo $this->Form->submit(__('Submit'), $formSubmitOptions); ?>
		</div>
</div>
</div>
