<?php //pr($subscribers); ?>
<div class="row subscription-list">
	<div class="col-md-10 col-md-offset-1">		
		<?php if(!$subscribers){ ?>
			<div class="row subscription-list__header_underline">
				<div class="col-md-12">					
					<h3>You have no subscribers!</h3>
				</div>
			</div>
		<?php } else { ?>
			<div class="row subscription-list__header_underline">
				<div class="col-md-12">	
					<h3>
						<?php
							$subscribersCount = count($subscribers);
							echo "$subscribersCount subscription" . (($subscribersCount > 1) ? 's' : '');
						?>
					</h3>
				</div>
			</div>
		<?php } ?>

		<?php if($subscribers): ?>
			<?php foreach($subscribers as $subscriber): ?>
				<div class="row subscription-list__subscription">
					<div class="col-md-2">
						<?php 
			                if(isset($subscriber['Profile']['pic_dir'])) {
			                    $pic_dir = $subscriber['Profile']['pic_dir'];
			                    $image = $this->Html->image($pic_dir, array(
			                        'alt' => 'Photo',
			                        'class' => 'img-rounded subscription-list__avatar'
			                    ));
			                } else {
			                    $image = $this->Html->image("//placehold.it/100", array(
			                        'alt' => 'Photo',
			                        'class' => 'img-rounded subscription-list__avatar'
			                    ));
			                }

			                $name = '';
			                $link = '';
			                if ($subscriber['Profile']['firstname']) {
			                    $name = $subscriber['Profile']['firstname'];
			                }
			                if ($subscriber['Profile']['lastname']) {
			                	if ($subscriber['Profile']['firstname']) {
			                		$name .= ' ';
			                	}
			                    $name .= $subscriber['Profile']['lastname'];                        
			                }
			                if($name) {  
			                    $link = $this->Html->link(
			                        h($name),
			                        array(
			                            'controller' => 'profiles',
			                            'action' => 'view',
			                            $subscriber['Profile']['id'],
			                            'comments' => false
			                        ),
			                        array('escape' => false)
			                    );
			                }               
			            ?>
				        <?php  
	                        echo $this->Html->link(
	                            $image,
	                            array(
	                                'controller' => 'profiles',
	                                'action' => 'view',
	                                $subscriber['Profile']['id'],
	                                'comments' => false
	                            ),
	                            array('escape' => false)
	                        );
	                    ?>
					</div>
					<div class="col-md-7">		
						<div class="row subscriber-list__name">
							<div class="col-md-6">
		                        <?php echo ($name ? $link : '&nbsp;'); ?>
							</div>
						</div>
					</div>
				</div>
			<?php endforeach ?>
		<?php endif ?>
	</div>
</div>