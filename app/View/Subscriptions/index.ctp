<div class="row subscription-list">
	<div class="col-md-10 col-md-offset-1">		
		<?php if(!$subscriptions){ ?>
			<div class="row subscription-list__header_underline">
				<div class="col-md-12">					
					<h3>You have no subscriptions!</h3>
				</div>
			</div>
		<?php } else { ?>
			<div class="row subscription-list__header_underline">
				<div class="col-md-12">	
					<h3>
						<?php
							$subscriptionsCount = count($subscriptions);
							echo "$subscriptionsCount subscription" . (($subscriptionsCount > 1) ? 's' : '');
						?>
					</h3>
				</div>
			</div>
		<?php } ?>

		<?php if($subscriptions): ?>
			<?php foreach($subscriptions as $subscription): ?>
			<?php //pr($subscription); ?>
				<div class="row subscription-list__subscription">
					<div class="col-md-2">
						<?php 
			                if(isset($subscription['Profile']['pic_dir'])) {
			                    $pic_dir = $subscription['Profile']['pic_dir'];
			                    $image = $this->Html->image($pic_dir, array(
			                        'alt' => 'Photo',
			                        'class' => 'img-rounded subscription-list__avatar'
			                    ));
			                } else {
			                    $image = $this->Html->image("//placehold.it/100", array(
			                        'alt' => 'Photo',
			                        'class' => 'img-rounded subscription-list__avatar'
			                    ));
			                }

			                $name = '';
			                $link = '';
			                if ($subscription['Profile']['firstname']) {
			                    $name = $subscription['Profile']['firstname'];
			                }
			                if ($subscription['Profile']['lastname']) {
			                	if ($subscription['Profile']['firstname']) {
			                		$name .= ' ';
			                	}
			                    $name .= $subscription['Profile']['lastname'];                        
			                }
			                if($name) {  
			                    $link = $this->Html->link(
			                        h($name),
			                        array(
			                            'controller' => 'profiles',
			                            'action' => 'view',
			                            $subscription['Profile']['id'],
			                            'comments' => false
			                        ),
			                        array('escape' => false)
			                    );
			                }               
			            ?>
				        <?php  
	                        echo $this->Html->link(
	                            $image,
	                            array(
	                                'controller' => 'profiles',
	                                'action' => 'view',
	                                $subscription['Profile']['id'],
	                                'comments' => false
	                            ),
	                            array('escape' => false)
	                        );
	                    ?>
					</div>
					<div class="col-md-7">		
						<div class="row subscription-list__name">
							<div class="col-md-6">
		                        <?php echo ($name ? $link : '&nbsp;'); ?>
							</div>
						</div>

						<!-- SUBSCRIBE/UNSUBSCRIBE BUTTON-->
						<?php
							echo $this->element('subscribe-button',
	                          	array(
	                            	'profile_author_id' => $subscription['Subscription']['profile_author_id'],
	                            	'buttonClasses' => 'btn btn-default subscribe-button_small'
	                          	)
	                        );
						?>
					</div>
				</div>
			<?php endforeach ?>
		<?php endif ?>
	</div>
</div>