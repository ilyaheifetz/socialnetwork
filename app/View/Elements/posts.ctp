<?php //pr($posts); ?>
<div class="posts <?php echo ((isset($stylesExternal)) ? $stylesExternal : ''); ?>">
    <div class="row">
        <div class="<?php echo $stylesInner; ?>">
            <?php if(!$posts) { ?>
                <div class="row posts__header_underline">
                    <div class="col-md-12 posts__header-count"> 
                        <h3>There are no <?php echo $postTitle; ?>s!</h3>
                    </div>
                </div>
            <?php } else { ?>
                <div class="row">
                    <div class="col-md-12 posts__header-count"> 
                        <h3>
                            <?php echo count($posts) . ' ' . $postTitle . 
                                ((count($posts) == 1) ? '' : 's');
                            ?> 
                        </h3>
                    </div>
                </div>
                <?php foreach($posts as $post): ?>
                    <div class="posts__post row" id="<?php echo 'post_id=' . $post['Post']['id']; ?>">
                        <?php //pr($post); ?>
                        <?php 
                            if(isset($post['Profile']['pic_dir'])) {
                                $pic_dir = $post['Profile']['pic_dir'];
                                $image = $this->Html->image($pic_dir, array(
                                    'alt' => 'Photo',
                                    'class' => 'img-rounded post__user-avatar-image'
                                ));
                            } else {
                                $image = $this->Html->image("//placehold.it/100", array(
                                    'alt' => 'Photo',
                                    'class' => 'img-rounded post__user-avatar-image'
                                ));
                            }                
                        ?>
                        <div class="col-md-2 post__user-avatar-wrapper">
                            <?php  
                                echo $this->Html->link(
                                    $image,
                                    array(
                                        'controller' => 'profiles',
                                        'action' => 'view',
                                        $post['Profile']['id'],
                                        'comments' => false
                                    ),
                                    array('escape' => false)
                                );
                            ?>
                        </div>
                        <?php
                            $name = '';
                            $link = '';
                            if ($post['Profile']['firstname']) {
                                $name = $post['Profile']['firstname'];
                            }
                            if ($post['Profile']['lastname']) {
                                if ($post['Profile']['firstname']) {
                                    $name .= ' ';
                                }
                                $name .= $post['Profile']['lastname'];                        
                            }
                            if($name) {  
                                $link = $this->Html->link(
                                    h($name),
                                    array(
                                        'controller' => 'profiles',
                                        'action' => 'view',
                                        $post['Profile']['id'],
                                        'comments' => false
                                    ),
                                    array('escape' => false)
                                );
                            } 
                        ?>
                        <div class="col-md-10">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel panel-post panel-default">
                                        <div class="panel-heading">
                                            <?php if($name): ?>
                                                <?php echo $link; ?>
                                            <?php endif ?>
                                            <span class="text-muted posts__post-created">
                                                <?php echo h($post['Post']['created']); ?>
                                            </span>
                                            <?php
                                                if (!empty($userId)) {
                                                    if (in_array($post['Post']['id'], $allLikesCurrentProfile)) {
                                            ?>
                                                <span class="like" data-id="<?php echo $post['Post']['id'] ?>">
                                                    <i class="fa fa-thumbs-o-up fa-2x"></i>
                                                    <span class="likes-count">
                                                        <?php echo count($post['Like'])?count($post['Like']):''; ?>            
                                                    </span>
                                                </span>
                                            <?php
                                                } else {
                                            ?>
                                                <span class="like _not-active" data-id="<?php echo $post['Post']['id'] ?>">
                                                    <i class="fa fa-thumbs-o-up fa-2x"></i>
                                                    <span class="likes-count">
                                                        <?php echo count($post['Like']) ? count($post['Like']) : ''; ?>            
                                                    </span>
                                                </span>
                                            <?php 
                                                    }
                                                }
                                            ?>

                                            <?php
                                                echo $this->element('post-toolbar', array(
                                                    'post' => $post['Post'],
                                                    'profileId' => $profileId,
                                                ));
                                            ?>                           
                                         
                                        </div>
                                        <div class="panel-body">
                                            <h4 class="posts__post-title">
                                                <?php
                                                    echo $link = $this->Html->link(
                                                        h($post['Post']['title']),
                                                        array(
                                                            'controller' => 'posts',
                                                            'action' => 'view',
                                                            $post['Post']['id'],
                                                            'comments' => false
                                                        ),
                                                        array('escape' => false)
                                                    );
                                                ?>
                                            </h4>                                            
                                            <hr class="post__undertitle-hr">
                                            <p class="posts__post-body"><?php echo str_replace("\n", '<br/>', h($post['Post']['body'])); ?></p>                                            
                                            <div class="post__under-the-post-body">
                                                <div class="posts__post-updated">
                                                    <?php if($post['Post']['created'] != $post['Post']['modified']): ?>
                                                        <div class="posts__post-updated-clock"><i class="fa fa-clock-o"></i></div>
                                                        <span class="text-muted posts__edit">Last updated <?php echo h($post['Post']['modified']); ?></span>
                                                    <?php endif ?>
                                                </div>
                                                <?php if ($post['Comment']): ?>
                                                    <div class="post__comments-count">
                                                        <?php 
                                                            echo $this->Html->link(
                                                                h(count($post['Comment']) . ((count($post['Comment']) == 1) ?
                                                                    ' Comment' : ' Comments')),
                                                                array(
                                                                    'controller' => 'posts',
                                                                    'action' => 'view',
                                                                    $post['Post']['id'],
                                                                    'comments' => false
                                                                ),
                                                                array('escape' => false)
                                                            );
                                                        ?>
                                                    </div>
                                                <?php endif ?>
                                            </div>                                                
                                        </div>
                                    </div>
                                </div>
                            </div>                              
                        </div>                         
                    </div>
                <?php endforeach ?>
            <?php } ?>
        </div>
    </div>
</div>