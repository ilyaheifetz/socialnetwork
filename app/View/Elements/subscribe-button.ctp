<?php
    $subscribeUnsubscribeButtonSettings = $this->requestAction(
        array(
            'controller' => 'profiles',
            'action' => 'getSubscribeUnsubscribeButtonSettings',
            $profile_author_id
        )
    );
?>                        
<div class="row">
    <div class="col-md-12">
        <div class="subscribe-button">                   
            <?php
                echo $this->Html->link(
                    $subscribeUnsubscribeButtonSettings['title'],
                    $subscribeUnsubscribeButtonSettings['url'],
                    array(
                        'class' => $buttonClasses,
                        'role' => 'button',
                        'id' => 'subscribeButton',
                        'data-profile-author-id' => $profile_author_id
                    )
                );
            ?>
        </div>
    </div>
</div>