<?php
    /*
     * Input params:
     * $profileId
     * $post
    */
?>

<?php if($profileId == $post['profile_id']): ?>
    <div class="post-toolbar">  
        <?php
            $editLink = $this->Html->link(
                //__('Edit'),
                '<i class="fa fa-pencil-square-o"></i>',
                array(
                    'controller' => 'posts',
                    'action' => 'edit',
                    $post['id'],
                    'comments' => false
                ),
                array('escape' => false)
            );

            $deleteLink = $this->Form->postLink(
                //__('Delete'),
                '<i class="fa fa-times"></i>',
                array(
                    'controller' => 'posts',
                    'action' => 'delete',
                    $post['id'],
                    'comments' => false
                ),
                array('escape' => false)
            );
        ?>
        <span class="post-toolbar__edit post-toolbar__margin-right"><?php echo $editLink; ?></span>
        <span class="post-toolbar__delete"><?php echo $deleteLink; ?></span>   
    </div>
<?php endif ?>