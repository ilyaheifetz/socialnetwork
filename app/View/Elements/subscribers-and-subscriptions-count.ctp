<?php    
    $subscribersCount = $this->requestAction(
        array(
            'controller' => 'profiles',
            'action' => 'getSubscribersCount',
            $profile_id
        )
    );
    $subscriptionsCount = $this->requestAction(
        array(
            'controller' => 'profiles',
            'action' => 'getSubscriptionsCount',
            $profile_id
        )
    );    
?>                   
<div class="row subscribers-and-subscriptions-count">
    <p class="subscribers-and-subscriptions-count__subscribers-title" <?php if($subscribersCount){ ?> style="display: inline" <?php } ?> >
        Subscribers <strong class="subscribers-and-subscriptions-count__subscribers-count"><?php echo $subscribersCount; ?></strong>
    </p>
    <p class="subscribers-and-subscriptions-count__subscriptions-title" <?php if($subscriptionsCount){ ?> style="display: inline" <?php } ?> >
        Subscriptions <strong class="subscribers-and-subscriptions-count__subscriptions-count"><?php echo $subscriptionsCount; ?></strong>
    </p>
</div>