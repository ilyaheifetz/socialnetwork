<div class="posts__comments">
	<div class="row">
	    <div class="col-md-12">
	        <?php //pr($comments); ?>
	        <?php foreach($comments as $comment): ?>
	        	<div class="row posts__comment row" id="<?php echo 'comment_id=' . $comment['id']; ?>">
	        		<?php 
                        if(isset($comment['Profile']['pic_dir'])) {
                            $pic_dir = $comment['Profile']['pic_dir'];
                            $image = $this->Html->image($pic_dir, array(
                                'alt' => 'Photo',
                                'class' => 'img-rounded img-responsive post__user-avatar-image'
                            ));
                        } else {
                            $image = $this->Html->image("//placehold.it/100", array(
                                'alt' => 'Photo',
                                'class' => 'img-rounded img-responsive post__user-avatar-image'
                            ));
                        }                
                    ?>
                    <div class="col-md-2 post__user-avatar-wrapper">
                        <?php  
                            echo $this->Html->link(
                                $image,
                                array(
                                    'controller' => 'profiles',
                                    'action' => 'view',
                                    $comment['Profile']['id'],
                                    'comments' => false
                                ),
                                array('escape' => false)
                            );
                        ?>
                    </div>
                    <?php
                        $name = '';
                        $link = '';
                        if ($comment['Profile']['firstname']) {
                            $name = $comment['Profile']['firstname'];
                        }
                        if ($comment['Profile']['lastname']) {
                            if ($comment['Profile']['firstname']) {
                                $name .= ' ';
                            }
                            $name .= $comment['Profile']['lastname'];                        
                        }
                        if($name) {  
                            $link = $this->Html->link(
                                h($name),
                                array(
                                    'controller' => 'profiles',
                                    'action' => 'view',
                                    $comment['Profile']['id'],
                                    'comments' => false
                                ),
                                array('escape' => false)
                            );
                        } 
                    ?>
                    <div class="col-md-10">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-post panel-default">
                                    <div class="panel-heading">
                                        <?php if($name): ?>
                                            <?php echo $link; ?>
                                        <?php endif ?>
                                        <span class="text-muted posts__post-created">
                                            <?php echo h($comment['created']); ?>
                                        </span>
                                        <?php
                                            if (!empty($userId)) {
                                                if (in_array($comment['id'], $allLikesCurrentProfile)) {
                                        ?>
                                            <span class="like" data-id="<?php echo $comment['id'] ?>">
                                                <i class="fa fa-thumbs-o-up fa-2x"></i>
                                                <span class="likes-count">
                                                    <?php echo count($comment['Like'])?count($comment['Like']):''; ?>            
                                                </span>
                                            </span>
                                            <?php
                                                } else {
                                            ?>
                                            <span class="like _not-active" data-id="<?php echo $comment['id'] ?>">
                                                <i class="fa fa-thumbs-o-up fa-2x"></i>
                                                <span class="likes-count">
                                                    <?php echo count($comment['Like']) ? count($comment['Like']) : ''; ?>            
                                                </span>
                                            </span>
                                        <?php 
                                                }
                                            }
                                        ?>

                                        <?php
                                            echo $this->element('post-toolbar', array(
                                                'post' => $comment,
                                                'profileId' => $profileId,
                                            ));
                                        ?>
                                        
                                    </div>
                                    <div class="panel-body">
                                        <h4 class="posts__post-title">
                                            <?php echo h($comment['title']); ?>
                                        </h4>
                                        <hr class="post__undertitle-hr">
                                        <p class="posts__post-body"><?php echo str_replace("\n", '<br/>', h($comment['body'])); ?></p>
                                        <div class="post__under-the-post-body">
                                            <div class="posts__post-updated">
                                                <?php if($comment['created'] != $comment['modified']): ?>
                                                    <div class="posts__post-updated-clock"><i class="fa fa-clock-o"></i></div>
                                                    <span class="text-muted posts__edit">Last updated <?php echo h($comment['modified']); ?></span>
                                                <?php endif ?>
                                            </div>                            
                                        </div>      
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>  
	        	</div>
	    	<?php endforeach ?>
	    </div>
	</div>
</div>