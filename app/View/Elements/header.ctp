<div class="header">
	<ul class="nav nav-pills pull-right header__link">
	 	<?php if(!AuthComponent::user()) { ?>
	  		<li><a href="/login">Log In</a></li>
	  	<?php } else { ?>
	  		<li class="dropdown">
	  			<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Profile <span class="caret"></span></a>
	  			<ul class="dropdown-menu" role="menu">
	                <li><a href="/profile">Edit Profile</a></li>
	                <li>
		                <?php 
		                	echo $this->Html->link(
		                		'View Profile',
		                		array(
		                			'controller' => 'profiles',
		                			'action' => 'view',
		                			ClassRegistry::init('Profile')->getProfileIdByUserId(AuthComponent::user('id'))
		                		)
		                	); 
		                ?>
	                </li>
                </ul>
	  		</li>
	  		<li><a href="/new_post">New Post</a></li>	  		
	  		<li>
	  			<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">My Subscriptions <span class="caret"></span></a>
	  			<ul class="dropdown-menu" role="menu">
	                <li><a href="/subscriptions">My Subscription List</a></li>	                
	                <li><a href="/subscription_posts">My Subscription Posts</a></li>
                </ul>
	  		</li>
	  		<li><a href="/subscribers">My Subscribers</a></li>	
	  		<li>
	  			<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Events<span class="caret"></span></a>
	  			<ul class="dropdown-menu" role="menu">
	                <li><a href="/new_event">Add Event</a></li>	                
	                <li><a href="/my_events">My Events</a></li>
	                <li><a href="/events">All Events</a></li>
                </ul>
	  		</li>
	  		<li><a href="/logout">Log Out</a></li>	  		
	  	<?php } ?> 
	</ul>
	<h3 class="text-muted header__projectName"><a class="header__projectName-link" href="/">Sociality</a></h3>
</div>