<div class="registration">
<h1 class="registration__title">Log In</h1>
	<div class="row">
	<!-- 	<div class="col-sm-2"></div> -->
		<?php 
			$formDesignOptions = array(
			'inputDefaults' => array(
			'div' => 'form-group',
			'label' => array(
			'class' => 'col col-sm-3 control-label'
			),
			'wrapInput' => 'col col-sm-6',
			'class' => 'form-control'
			),
			'class' => 'form-horizontal col-sm-8 col-centered registration__center'
			);
			$formSubmitOptions = array(
			'div' => 'col col-sm-offset-3 col-sm-6',
			'class' => 'btn btn-primary'
			);
		?>
		<?php echo $this->Session->flash('auth'); ?>
		<?php echo $this->Form->create('User', $formDesignOptions); ?>
		    <fieldset>
		        <legend>
		            <?php echo __('Please enter your e-mail and password'); ?>
		        </legend>
		        <?php echo $this->Form->input('email');
		        echo $this->Form->input('password');
		    ?>
		    </fieldset>
		    <div class="form-group">
		    	<?php echo $this->Form->submit('Log In', $formSubmitOptions); ?>
			</div>
		<!-- <div class="col-sm-2"></div> -->
	</div>
</div>
