<div class="main">
	<?php if($this->Session->flash('registered')) { ?>
	<div class="jumbotron main__count">
		<h2 class="registration__title">Please Confirm Your Email Address to Complete Your Registration</h2>
		<p class="lead">Please check your email and confirm your email address to complete the registration process!</p>
	</div>
<?php } else { ?>

<?php if($this->Session->flash('confirmed')) { ?>
	<div class="jumbotron main__count">
		<h2>Your e-mail has now been confirmed</h2>
		<p class="lead">
			Congratulations! Your e-mail has now been confirmed. You can now 
			<?php echo $this->Html->link(
			    'login',
			    array(
			    	'action' => 'login'
			    )
			);
			?>
			to your account.
		</p>
	</div>
<?php } else { ?>

	<div class="registration">
		<h1 class="registration__title">Sign Up</h1>
	<br />
		<div class="row">
			<div class="col-sm-2"></div>
				<?php 
					$formDesignOptions = array(
					'inputDefaults' => array(
					'div' => 'form-group',
					'label' => array(
					'class' => 'col col-sm-3 control-label'
					),
					'wrapInput' => 'col col-sm-6',
					'class' => 'form-control'
					),
					'class' => 'form-horizontal col-sm-8 col-centered registration__center'
					);
					$formSubmitOptions = array(
					'div' => 'col col-sm-offset-3 col-sm-6',
					'class' => 'btn btn-primary'
					);
				?>
				<?php 
					echo $this->Form->create('User', $formDesignOptions);
				?>
				<fieldset>
				<legend><?php echo __('Please enter your e-mail and password'); ?></legend>
				<?php
					echo $this->Form->input('email', array('placeholder' => 'Email'));
					echo $this->Form->input('password', array('placeholder' => 'Password'));
				 ?>
				</fieldset>
				<div class="form-group">
					<?php echo $this->Form->submit('Sing Up', $formSubmitOptions); ?>
				</div>
			<div class="col-sm-2"></div>
		</div>
	</div>
<?php }} ?>
</div>
