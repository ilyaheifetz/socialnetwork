<div class="main">      
      <div class="jumbotron main__count">
        <h2>Count of registered users <?php echo $usersCount; ?></h2>
        <p class="lead">Find friends, share photos with them, play games, watch interesting videos, listen to the best music</p>
        <p><a class="btn btn-lg btn-primary" href="/registration" role="button">Sign up today</a></p>
      </div>
      <div class="row">
        <?php $i=0;?>
        <?php foreach ($usersProfile as $profile) { ?>
        <div class="col-xs-6">
          <?php 
            $i=$i+1;
            if ($i < 2) {
          ?>
            <div class="main_users">
              <?php 
                if(isset($profile['Profile']['pic_dir'])) {
                  $pic_dir = $profile['Profile']['pic_dir'];
                  echo $this->Html->link(
                  $this->Html->image($pic_dir, array(
                  'alt' => 'Photo',
                  'class' => 'main_avatar img-circle'
                )),
                array(
                  'controller' => 'profiles',
                  'action' => 'view',
                  $profile['Profile']['id'],
                  'comments' => false
                ),
                array('escape' => false)
                );
                } else {
                  echo $this->Html->image("//placehold.it/100", array(
                    'alt' => 'Photo',
                    'class' => 'main_avatar img-circle'
                  ));
                }
              ?>

              <?php
              /*
                echo $this->Html->image('/files/profile/userpic/' . $profile['Profile']['userpic_dir'] . '/' . $profile['Profile']['userpic'],
                  array('class' => 'main_avatar img-circle'));
              */
              ?>
              <div class="caption">
                <!--h4 class="main_name"><?php echo $profile['Profile']['firstname'] . ' ' . $profile['Profile']['lastname'] ?></h4-->
                <h4 class="main_name">
                  <?php
                    echo $this->Html->link(
                      $profile['Profile']['firstname'] . ' ' . $profile['Profile']['lastname'],
                      array(
                        'controller' => 'profiles',
                        'action' => 'view',
                        $profile['Profile']['id'],
                        'comments' => false
                      ),
                      array('escape' => false)
                    );
                  ?>
                </h4>
              </div>
            </div>
          </div>
      <div class="col-xs-6">
        <?php } else { ?>
        <div class="main_users">
            <?php 
                if(isset($profile['Profile']['pic_dir'])) {
                $pic_dir = $profile['Profile']['pic_dir'];
                echo $this->Html->link(
                  $this->Html->image($pic_dir, array(
                  'alt' => 'Photo',
                  'class' => 'main_avatar img-circle'
                )),
                array(
                  'controller' => 'profiles',
                  'action' => 'view',
                  $profile['Profile']['id'],
                  'comments' => false
                ),
                array('escape' => false)
                );
              // if(isset($profile['Profile']['pic_dir'])) {
              //   $pic_dir = $profile['Profile']['pic_dir'];
              //   echo $this->Html->image($pic_dir, array(
              //     'alt' => 'Photo',
              //     'class' => 'main_avatar img-circle'
              //   ));
              } else {
                echo $this->Html->image("//placehold.it/100", array(
                  'alt' => 'Photo',
                  'class' => 'main_avatar img-circle'
                ));
              }
            ?>
            <div class="caption">
              <h4 class="main_name">
                <?php
                  echo $this->Html->link(
                    $profile['Profile']['firstname'] . ' ' . $profile['Profile']['lastname'],
                    array(
                      'controller' => 'profiles',
                      'action' => 'view',
                      $profile['Profile']['id'],
                      'comments' => false
                    ),
                    array('escape' => false)
                  );
                ?>
              </h4>
            </div>
          </div>
        <?php   } ?>
      </div>
      <?php } ?>
    </div>

    <?php 
      echo $this->element('posts', array(
        'stylesInner' => 'col-md-12',
        'posts' => $posts,
        'postTitle' => 'Post',
        'userId' => $userId,
        'allLikesCurrentProfile' => $allLikesCurrentProfiles
      )); 
    ?>    
</div>

<?php //echo $this->element('sidebar'); ?> 
