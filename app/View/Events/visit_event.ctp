<div class="row">
	<div class="col-md-8 col-md-offset-2">
<?php 
	foreach ($profile['Visiting'] as $event) { ?>
			<div class="event">
			  	<h3><b><?php echo $event['title']; ?></b></h3>
		  		<p><?php echo $event['body']; ?></p>
		  		<p>
		  			<?php 
			  			echo $this->Html->link('View more', array(
			  				'controller' => 'events', 'action' => 'view', $event['id']
			  				)
			  			); 
		  			?>
		  		</p>
			</div>
<?php	}
?>
	</div>
</div>