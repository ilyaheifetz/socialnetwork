<div class="row">
	<div class="col-md-8 col-md-offset-4">
		<h2>Count of events <?php echo count($events)?></h2>
	</div>
</div>
<div class="row">
	<div class="col-md-8 col-md-offset-2">
		<?php if($events): ?>
			<?php foreach($events as $event): ?>
				<div class="event">
				  	<h3><b><?php echo $event['Event']['title']; ?></b></h3>
			  		<p><?php echo $event['Event']['body']; ?></p>
			  		<p>
			  			<?php 
				  			echo $this->Html->link('View more', array(
				  				'controller' => 'events', 'action' => 'view', $event['Event']['id']
				  				)
				  			); 
			  			?>
			  		</p>
				</div>
			<?php endforeach ?>
		<?php endif ?>
	</div>
</div>
