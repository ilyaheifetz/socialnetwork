<br />

<div class="row">
	<div class="col-md-2">
		<?php 
            if(isset($event['Profile']['pic_dir'])) {
                $pic_dir = $event['Profile']['pic_dir'];
                $image = $this->Html->image($pic_dir, array(
                    'alt' => 'Photo',
                    'class' => 'img-rounded post__user-avatar-image'
                ));
            } else {
                $image = $this->Html->image("//placehold.it/100", array(
                    'alt' => 'Photo',
                    'class' => 'img-rounded post__user-avatar-image'
                ));
            }                
        ?>
        <div class="col-md-2 post__user-avatar-wrapper">
            <?php  
                echo $this->Html->link(
                    $image,
                    array(
                        'controller' => 'profiles',
                        'action' => 'view',
                        $event['Profile']['id'],
                        'comments' => false
                    ),
                    array('escape' => false)
                );
            ?>
        </div>
	</div>
	<div class="col-md-6">
		<div class="panel panel-info">
		  <!-- Default panel contents -->
		  <div class="panel-heading">
		  	<?php
		  		echo str_replace("\n", '<br/>', h($event['Event']['title']));
            ?>
		  </div>
		  <div class="panel-body">
		    <p>
		    	<?php echo str_replace("\n", '<br/>', h($event['Event']['body'])); ?>
		    </p>
		    <p>Начало: 
		    	<?php echo str_replace("\n", '<br/>', h($event['Event']['date'])); ?>	
		    </p>
		  </div>
		</div>
	</div>
	<div class="col-md-3">
        <?php 
        if (!empty($profile)) {
            if($profile['Profile']['id'] != $event['Event']['profile_id']) {
                $visit = false;
                foreach ($profile['Visiting'] as $prof) {
                    if($prof['id'] == $event['Event']['id']) {
                        $visit = true;
                    }
                }
                if($visit) {            
                    echo $this->Html->link('not visit event', array(
                        'controller' => 'profiles', 'action' => 'notVisitEvent', $event['Event']['id']
                        )
                    );
                } else {
                    echo $this->Html->link('visit event', array(
                        'controller' => 'profiles', 'action' => 'visitEvent', $event['Event']['id']
                        )
                    );
                }
            }
        }
        ?>
    </div>
    <div class="row">
        <?php if(empty($event['Guest'])) { ?>
            <div class="col-md-8 col-md-offset-3">
                <h2>At this event no guests</h2>
                <br />
            </div>
    <?php    } else { ?>
        <div class="col-md-12 col-md-offset-5">
            <h2>Guests</h2>
        </div>
        <div class="col-md-11 col-md-offset-1">
            <?php foreach ($event['Guest'] as $guest) { ?>
                <div class="row">
                    <div class="col-md-2">
                        <?php 
                            if(isset($guest['pic_dir'])) {
                              $pic_dir = $guest['pic_dir'];
                              echo $this->Html->link(
                              $this->Html->image($pic_dir, array(
                              'alt' => 'Photo',
                              'class' => 'img-rounded subscription-list__avatar'
                            )),
                            array(
                              'controller' => 'profiles',
                              'action' => 'view',
                              $guest['id'],
                              'comments' => false
                            ),
                            array('escape' => false)
                            );
                            } else {
                              echo $this->Html->image("//placehold.it/100", array(
                                'alt' => 'Photo',
                                'class' => 'img-rounded subscription-list__avatar'
                              ));
                            }
                          ?>    
                    </div>
                    <div class="col-md-8">
                        <div class="row">
                            <?php echo $guest['firstname']; ?>
                            <?php echo $guest['lastname']; ?>
                        </div>
                    </div>
                </div>
                <br />
            <?php } 
        } ?>
        </div>
    </div>
</div>