<div class="row subscription-list">
	<div class="col-md-10 col-md-offset-1">		
		<?php if(!$events){ ?>
			<div class="row subscription-list__header_underline">
				<div class="col-md-12">					
					<h3>  You have no events!</h3>
				</div>
			</div>
		<?php } else { ?>
			<div class="container">
				<div class="row row-offcanvas row-offcanvas-right">
				    <div class="col-xs-8">
				      <p class="pull-right visible-xs">
				        <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button>
				      </p>
				      <div class="row">
				      			<?php if($events): ?>
						<?php foreach($myEvents as $event): ?>
				        <div class="col-4 col-sm-4">
				          <h2><?php echo $event['Event']['title']; ?></h2>
				          <p><?php echo $event['Event']['body']; ?></p>
				          <p><?php echo $event['Event']['date']; ?></p>
				        </div><!--/span-->
						<?php endforeach ?>
					<?php endif ?>
				      </div><!--/row-->
				    </div><!--/span-->
				</div><!--/row-->
			</div><!--/.container-->
		<?php } ?>
	</div>
</div>