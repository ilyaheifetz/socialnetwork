<?php if(!$myEvents){ ?>
	<div class="row">
		<div class="col-md-12">					
			<h3>&nbsp;&nbsp;You have no events!</h3>
		</div>
	</div>
<?php } else { ?>
	<div class="container">
		<div class="row row-offcanvas row-offcanvas-right">
		    <div class="col-md-8">
<!-- 		      <p class="pull-right visible-xs">
		        <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button>
		      </p> -->
		      <div class="row">
		      			<?php if($myEvents): ?>
				<?php foreach($myEvents as $event): ?>
		        <div class="col-md-11 col-md-offset-1">
		          <h2><?php echo $event['Event']['title']; ?></h2>
		          <p><?php echo $event['Event']['body']; ?></p>
		          <p>Начало: <?php echo $event['Event']['date']; ?>
		          	<span class="event__view-event">
		          <?php 
			  			echo $this->Html->link('View more', array(
			  				'controller' => 'events', 'action' => 'view', $event['Event']['id']
			  				)
			  			); 
		  			?></span></p>
		        </div><!--/span-->
				<?php endforeach ?>
			<?php endif ?>
		      </div><!--/row-->
		    </div><!--/span-->
		</div><!--/row-->
	</div><!--/.container-->
<?php } ?>
