<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $this->fetch('title'); ?>
	</title>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<?php
		echo $this->Html->meta('icon');

		echo $this->Html->css('bootstrap.min.css');
		echo $this->Html->css('bootstrap-theme.min');
		//echo $this->Html->css('font-awesome.min');
		echo $this->Html->css('//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css');
		echo $this->Html->css('main');
		// echo $this->Html->script('jquery-1.11.2.min');
		// echo $this->Html->js('bootstrap');
		// echo $this->Html->js('bootstrap.min');

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');


		echo $this->Html->script('bootstrap');
		echo $this->Html->script('bootstrap.min');
		echo $this->Html->script('main');

	?>

</head>
<body>
	<div class="container">
		<div class="page">
			<?php echo $this->element('header'); ?>
			<?php echo $this->Session->flash(); ?>
			<?php echo $this->fetch('content'); ?>
		</div>
		<?php echo $this->element('footer'); ?>
	</div>
	<?php echo $this->element('sql_dump'); ?>

	<script src="/js/bootstrap.min.js"></script>
	<script src="/js/subscribe.js"></script>
	<script src="/js/ajaxloader.js"></script>
	<script src="/js/like.js"></script>
</body>
</html>

