<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Emails.text
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>
<?php
	echo 'Thanks for signing up!' . '\r\n';
	echo '\r\n';
	echo 'Please click the link below to confirm your registration.' . '\r\n';
	echo '\r\n' . '\r\n';
	echo '---' . '\r\n';
	echo 'CONFIRM BY VISITING THE LINK BELOW:' . '\r\n' . '\r\n ';
	echo 'http://socialnetwork/registration/confirm/' . $user['User']['emailtoken']. ' \r\n' . '\r\n';
	echo '---' . '\r\n';
	echo 'If you do not want to confirm, simply ignore this message.';
?>