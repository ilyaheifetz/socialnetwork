<?php 
	$formDesignOptions = array(
		'inputDefaults' => array(
			'div' => 'form-group',
			'label' => array(
				'class' => 'col col-lg-3 control-label'
			),
			'wrapInput' => 'col col-lg-8',
			'class' => 'form-control'
		),
		'class' => 'form-horizontal col-lg-12 col-centered',
		'type' => 'file'
	);
	$formSubmitOptions = array(
	'div' => 'col col-sm-offset-3 col-md-8',
	'class' => 'btn btn-primary'
	);
?>
<h1 class="profile__title">Personal info</h1>
<hr>
	<div class="row">
 		<div class="col-md-5">
 			<div class="profile__loadfhoto">
		        <!--img src="//placehold.it/100" class="avatar img-thumbnail" alt="avatar"-->
		        
		        <?php 
		        	if(isset($profile['Profile']['pic_dir'])) {
		        		$pic_dir = $profile['Profile']['pic_dir'];
		        		echo $this->Html->image($pic_dir, array(
		        			'alt' => 'Photo',
		        			'class' => 'avatar img-thumbnail'
		        		));
		        	} else {
		        		echo $this->Html->image("//placehold.it/100", array(
		        			'alt' => 'Photo',
		        			'class' => 'avatar img-thumbnail'
		        		));
		        	}
		        ?>
		        <h6>Upload a different photo...</h6>
		    </div>
	    </div>
	    <div class="col-md-7 personal-info">
	        	<?php echo $this->Form->create('Profile', $formDesignOptions);
	            	echo $this->Form->input('firstname');
	            	echo $this->Form->input('lastname');
	            	echo $this->Form->input('username');
	            	echo $this->Form->input('age', array(
	            			'type' => 'date',
	            			'minYear' => 1920,
	            			'maxYear' => date('Y', time())
	            			)
	            	);
	            	echo $this->Form->input('bio');
	            	echo $this->Form->input('userpic', array(
		        		'class' => 'form-group',
		        		'type' => 'file',
		        		'label' => 'Photo'
		        	));
		        	echo $this->Form->input('userpic_dir', array('type' => 'hidden'));
				?>
			<div class="form-group">
	            <?php 
	            	echo $this->Form->submit('Save', $formSubmitOptions);
				?>
	      	</div>
	  	</div>
	</div>
<hr>
