<div class="main">
	<div class="container">
      <div class="row">
        <div class="col-sm-7 main_indent">
 
   
          <div class="panel panel-info">
            <div class="panel-heading">
              <h3 class="panel-title">Profile</h3>
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="col-md-3 col-lg-3 " align="center">
                	<?php 
                    if(isset($profile['Profile']['pic_dir'])) {
                  		$pic_dir = $profile['Profile']['pic_dir'];
                  		echo $this->Html->image($pic_dir, array(
  		                  'alt' => 'Photo',
  		                  'class' => 'img-circle'
  		                ));
                    } else {
                      echo $this->Html->image("//placehold.it/100", array(
                        'alt' => 'Photo',
                        'class' => 'img-circle'
                      ));
                    }
                	?>
                  
                  <div>
                    <!-- SUBSCRIBERS/SUBSCRIPTIONS COUNT-->
                    <?php echo $this->element('subscribers-and-subscriptions-count', array('profile_id' => $profile['Profile']['id'])); ?>                   

                    <!-- SUBSCRIBE/UNSUBSCRIBE BUTTON-->
                    <?php
                      if(AuthComponent::user() && ($profile['Profile']['id'] != AuthComponent::user('Profile.id'))) {
                        echo $this->element('subscribe-button',
                          array(
                            'profile_author_id' => $profile['Profile']['id'],
                            'buttonClasses' => 'btn btn-info subscribe-button_large'
                          )
                        );
                      }
                    ?>
                  </div>
                </div>
                
                <div class=" col-md-9 col-lg-9 "> 
                  <table class="table table-user-information">
                    <tbody>
                      <tr>
                        <td>Name:</td>
                        <td><?php echo $profile['Profile']['firstname'] . ' ' . $profile['Profile']['lastname'] ?></td>
                      </tr>
                      <tr>
                        <td>Username:</td>
                        <td><?php echo $profile['Profile']['username'] ?></td>
                      </tr>
                      <tr>
                        <td>Age</td>
                        <td><?php 
                              $date = strtotime ($profile['Profile']['age']);
                              $age=floor((date('U')-$date)/31536000);
                              echo $age;
                            ?>
                          </td>
                      </tr>
                      <tr>
                        <td>Bio</td>
                        <td><?php echo $profile['Profile']['bio']?></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>           
          </div>
      </div>
    </div>
</div>