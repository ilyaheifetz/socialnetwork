<?php
App::uses('AppModel', 'Model');
/**
 * Post Model
 *
 */
class Post extends AppModel {

	public $belongsTo = array('Profile',
		'Parent' => array(
			'className' => 'Post',
			'foreignKey' => 'parent_id'
		)
	);
	public $hasMany = array('Like',
		'Comment' => array(
			'className' => 'Post',
			'foreignKey' => 'parent_id'
		)
	);



/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'title';

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'id';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'profile_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'title' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'body' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	public function getAllPosts() {
		$params = array(
			'conditions' => array(
				'Post.parent_id' => NULL
			),
			'order' => 'Post.created DESC',
		);
		$posts = $this->find('all', $params);
		return $posts;
	}

	public function getPostWithComments($id = null) {
		if(!$id) {
			return false;
		}
		$params = array(
			'conditions' => array(
				'Post.id' => $id
			),
			'recursive' => 2,
			//'order' => 'Post.created DESC',
		);
		$postWithComments = $this->find('first', $params);
		return $postWithComments;
	}

	public function getPostsWithReverseComments($posts = null) {
		if(!$posts) {
			return $posts;
		}
		foreach($posts as &$post){
			if($post['children'] && count($post['children']) > 1) {
				$post['children'] = array_reverse($post['children']);
			}
		}
		return $posts;
	}

	public function getSubscriptionPostsByProfileIdsArray($profileIdsArray) {
		if(!$profileIdsArray) {
			return false;
		}
		if(count($profileIdsArray) == 0) {
			return false;
		}
		$params = array(
			'order' => array('Post.created DESC'),
			'conditions' => array(
				'Post.profile_id' => $profileIdsArray,
				'Post.parent_id' => NULL
			)
		);
		$subscriptionPosts = $this->find('all', $params);
		if(!$subscriptionPosts) {
			return false;
		}
		return $subscriptionPosts;
	}

	public function getPostAuthor($profileId, $subscriptionPost) {
		foreach ($subscriptionPost as $sub) {
			$subscribePost[] = $this->find('threaded', array(
				'conditions' => array(
					'Post.profile_id' => $sub['Subscription']['profile_author_id']
				),
			));
		}
		return $subscribePost;
	}

	public function getCommentPostsById($id) {
		$options = array(
			'conditions' => array('Post.id' => $id),
			'order' => array('Post.created')
		);
		$commentPosts = $this->find('all', $options);
		return $commentPosts;
	}

	public function getPostProfileIdByPostId($id) {
		$options = array(
			'conditions' => array('Post.id' => $id)
		);
		$post = $this->find('first', $options);
		$postProfileId = $post['Post']['profile_id'];
		return $postProfileId;
	}

	public function getPostParentIdByPostId($id) {
		$post = $this->findById($id);
		$postParentId = $post['Post']['parent_id'];
		if($postParentId) {
			return $postParentId;
		}
		return false;
	}
}
