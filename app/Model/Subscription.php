<?php
App::uses('AppModel', 'Model');
/**
 * Subscription Model
 *
 */
class Subscription extends AppModel {

	public $belongsTo = array(
		'Profile' => array(
			'className' => 'Profile',
			'foreignKey' => 'profile_author_id'
		),
		/*
		'ProfileReader' => array(
			'className' => 'Profile',
			'foreignKey' => 'profile_reader_id'
		)
		*/
	);

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'id';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'profile_author_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'profile_reader_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	public function addSubscription($profile_author_id, $profile_reader_id) {
		if ($profile_author_id == $profile_reader_id) {
			return false;
		}
		$this->create();
		$this->set('profile_author_id', $profile_author_id);
		$this->set('profile_reader_id', $profile_reader_id);
		return ($this->save());
	}

	public function deleteSubscription($profile_author_id, $profile_reader_id) {
		$subscription = $this->findSubscription($profile_author_id, $profile_reader_id);
		$this->id = $subscription['Subscription']['id'];
		return ($this->delete());
	}

	public function findSubscription($profile_author_id, $profile_reader_id) {
		$options = array(
			'conditions' => array(
				'profile_author_id' => $profile_author_id,
				'profile_reader_id' => $profile_reader_id
			)	
		);
		return $this->find('first', $options);
	}

	public function existsSubscription($profile_author_id, $profile_reader_id) {
		$options = array(
			'conditions' => array(
				'profile_author_id' => $profile_author_id,
				'profile_reader_id' => $profile_reader_id
			)	
		);
		if (!$this->find('first', $options)) {
			return false;
		}
		return true;
	}

	public function getSubscriptionsCount($profile_id) {
		$subscriptionsCount = $this->find('count', array(
			'conditions' => array('profile_reader_id' => $profile_id)
		));
		return $subscriptionsCount;
	}

	public function getSubscribersCount($profile_id) {
		$subscribersCount = $this->find('count', array(
			'conditions' => array('profile_author_id' => $profile_id)
		));
		return $subscribersCount;
	}

	public function getSubscriptionsByProfileReaderId($profile_reader_id) {
		$subscriptions = $this->find('all', array(
			'conditions' => array('profile_reader_id' => $profile_reader_id)
		));
		return $subscriptions;
	}

	public function getSubscribersByProfileReaderId($profile_author_id) {
		$this->belongsTo['Profile']['foreignKey'] = 'profile_reader_id';
		$subscribers = $this->find('all', array(
			'conditions' => array('profile_author_id' => $profile_author_id)
		));
		return $subscribers;
	}

	public function getSubscriptionProfileAuthorIdsArrayByProfileReaderId($profile_reader_id) {
		$subscriptionProfileAuthorIds = $this->find('all', array(
			'conditions' => array('profile_reader_id' => $profile_reader_id),
			'fields' => array('profile_author_id')
		));

		if(!$subscriptionProfileAuthorIds) {
			return false;
		}
		$subscriptionProfileAuthorIdsArray = array();
		foreach($subscriptionProfileAuthorIds as $record) {
			$subscriptionProfileAuthorIdsArray[] = $record['Subscription']['profile_author_id'];
		}
		if(!$subscriptionProfileAuthorIdsArray) {
			return false;
		}
		return $subscriptionProfileAuthorIdsArray;
	}
}
