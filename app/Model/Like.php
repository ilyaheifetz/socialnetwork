<?php
App::uses('AppModel', 'Model');
/**
 * Post Model
 *
 */
class Like extends AppModel {

	public $belongsTo = array('Profile', 'Post');



/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'title';

/**
 * Primary key field
 *
 * @var string
 */


/**
 * Validation rules
 *
 * @var array
 */

	public function checkLike($postId, $profileId) {
		$checkLike = $this->find('first', array(
			'conditions' => array(
				'Like.post_id' => $postId,
				'Like.profile_id' => $profileId,
				)
			)
		);
		if (!empty($checkLike)) {
			return true;
		} else {
			return false;
		}
	}

	public function addLike($postId, $profileId) {
		$this->set('profile_id', $profileId);
		$this->set('post_id', $postId);
		return $this->save();
	}

	public function deleteLike($postId, $profileId) {
		$options = array(
			'conditions' => array(
				'Like.post_id' => $postId,
				'Like.profile_id' => $profileId,
				)
			);
		$currentLike = $this->find('first', $options);
		$this->id = $currentLike['Like']['id'];
		return $this->delete();
	}

	public function likingPosts($profileId) {
		$currentProfileLikingPosts = $this->find('all', array(
			'conditions' => array(
				'Like.profile_id' => $profileId,
				),
			)
		);
		return $currentProfileLikingPosts;
	}

	public function countLike($postId) {
		$countLike = $this->find('count', array(
			'conditions' => array('post_id' => $postId)
		));
		return $countLike;
	}
}
