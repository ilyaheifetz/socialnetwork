<?php
App::uses('AppModel', 'Model');
/**
 * Profile Model
 *
 */
class Profile extends AppModel {

	public $actsAs = array(
        'Upload.Upload' => array(
            'userpic' => array(
                'fields' => array(
                    'dir' => 'userpic_dir'
                ),
                'thumbnailSizes' => array(
                    'thumb' => '100x100'
                ),
            )
        )
    );

	public $belongsTo = array('User');
	public $hasMany = array(
		'Post',
		'Like',
		'ProfiledAuthor' => array(
			'className' => 'Subscription',
			'foreignKey' => 'profile_author_id'
		),
		'ProfilefReader' => array(
			'className' => 'Subscription',
			'foreignKey' => 'profile_reader_id'
		),
		'Event' => array(
            'className' => 'Event',
            'foreignKey' => 'profile_id'
        )
	);
	public $hasAndBelongsToMany = array(
		'Visiting' => array(
                'className' => 'Event',
            )
        );

	//public $hasMany = array('Subscription');

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'id';

	public $virtualFields = array(
	    //'pic_dir' => "CONCAT('/" . WEBROOT_DIR . "/files/profile/userpic/', Profile.userpic_dir, '/', Profile.userpic)";
	    'pic_dir' => "CONCAT('/app/webroot/files/profile/userpic/', userpic_dir, '/thumb_', userpic)"
	    //'pic_dir' => 'CONCAT(userpic_dir, "/", Profile.userpic)'
	);

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'user_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'username' => array(
			'validCharacters' => array(
                'rule' => '/^[a-zа-я0-9_-]+$/msiu',
                'allowEmpty' => true,
                'message' => 'Usernames can only contain letters, numbers, hyphens and underscores'
            ),
		    'unique' => array(
		        'rule' => 'isUnique',
		        'message' => 'This username already exists. Please choose a different username'
		    ),
		),
		'firstname' => array(
			'validCharacters' => array(
				'rule' => '/^[a-zа-я ,.\'-]+$/msiu',
				'message' => 'Invalid Firstname',
				'allowEmpty' => true
			),
		),
		'lastname' => array(
			'validCharacters' => array(
				'rule' => '/^[a-zа-я ,.\'-]+$/msiu',		
				'message' => 'Invalid Lastname',
				'allowEmpty' => true
			),
		),		
		'userpic' => array(
	        'rule' => array('isValidExtension', array('png', 'jpg', 'bmp', 'gif'), false),
	        'message' => 'Please supply a valid image.',
	        'allowEmpty' => true
	    ),
	    
	);

	public function getLastProfiles() {
		$all = $this->find('all', array(
        		'conditions' => array(
        			'Profile.active' => 1
        			),
        		'order' => array(
        			'Profile.id' => 'DESC'
        			),
        		'limit' => 10,
        		)
		);
		//pr($all);
		return $all;
	}

	public function getProfileIdByUserId($user_id)
	{
		$options = array(
			//'conditions' => array('User.' . $this->User->primaryKey => $user_id
			'conditions' => array('Profile.user_id' => $user_id)	
		);
		$profile = $this->find('first', $options);
		if (!empty($profile)) {
			return $profile['Profile']['id'];
		}
	}

	public function afterSave($created, $options = array()) {
		$savedRecord = $this->findById($this->id);
		foreach($savedRecord['Profile'] as $field) {
			if(is_null($field) || $field === '') {				
				$this->saveField('active', 0, $params = array('callbacks' => false));				
				return true;
			}			
		}		
		$this->saveField('active', 1, $params = array('callbacks' => false));				
		return true;
	}

	// remove leading and trailing whitespace from posted data
	public function beforeValidate($options = array()) {
        if (!function_exists('trimItem')) {
            function trimItem(&$item,$key){
                if (is_string($item)){
                    $item = trim($item);    
                }
            }
        }       
        array_walk_recursive($this->data, 'trimItem');
	}

}
