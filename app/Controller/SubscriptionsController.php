<?php
App::uses('AppController', 'Controller');
/**
 * Subscriptions Controller
 *
 * @property Subscription $Subscription
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class SubscriptionsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $uses = array('Subscription', 'Profile');
	public $components = array('Paginator', 'Session');

	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->deny('index');
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$profile_reader_id = $this->Profile->getProfileIdByUserId($this->Auth->user('id')); 
		$this->set('subscriptions', $this->Subscription->getSubscriptionsByProfileReaderId($profile_reader_id));
	}

	public function subscribers() {
		$profile_author_id = $this->Profile->getProfileIdByUserId($this->Auth->user('id'));
		$this->set('subscribers', $this->Subscription->getSubscribersByProfileReaderId($profile_author_id)); 	
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Subscription->exists($id)) {
			throw new NotFoundException(__('Invalid subscription'));
		}
		$options = array('conditions' => array('Subscription.' . $this->Subscription->primaryKey => $id));
		$this->set('subscription', $this->Subscription->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Subscription->create();
			if ($this->Subscription->save($this->request->data)) {
				$this->Session->setFlash(__('The subscription has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The subscription could not be saved. Please, try again.'));
			}
		}
		$profiles = $this->Subscription->Profile->find('list');
		$this->set(compact('profiles'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Subscription->exists($id)) {
			throw new NotFoundException(__('Invalid subscription'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Subscription->save($this->request->data)) {
				$this->Session->setFlash(__('The subscription has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The subscription could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Subscription.' . $this->Subscription->primaryKey => $id));
			$this->request->data = $this->Subscription->find('first', $options);
		}
		$profiles = $this->Subscription->Profile->find('list');
		$this->set(compact('profiles'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Subscription->id = $id;
		if (!$this->Subscription->exists()) {
			throw new NotFoundException(__('Invalid subscription'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Subscription->delete()) {
			$this->Session->setFlash(__('The subscription has been deleted.'));
		} else {
			$this->Session->setFlash(__('The subscription could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
