<?php
App::uses('AppController', 'Controller');
/**
 * Profiles Controller
 *
 * @property Profile $Profile
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class ProfilesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $uses = array('Profile', 'User', 'Subscription', 'Like', 'Post', 'Event');
	public $components = array('Paginator', 'Session', 'RequestHandler');

	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('getSubscribersCount', 'getSubscriptionsCount');
	}

/**
 * index method
 *
 * @return void
 */

	public function index() {
		$this->Profile->recursive = 0;
		$this->set('profiles', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */

	public function view($id = null) {
		if (!$this->Profile->exists($id)) {
			throw new NotFoundException(__('Invalid profile'));
		}
		//pr($this);
		$options = array('conditions' => array('Profile.' . $this->Profile->primaryKey => $id));
		$this->set('profile', $this->Profile->find('first', $options));
	}

	public function getSubscribeUnsubscribeButtonSettings($profile_author_id) {
		if(!($profile_reader_id = $this->getAuthorizatedProfileId())) {
			return false;
		}
		if($profile_reader_id == $profile_author_id) {
			return false;
		}
		//$SubscribeUnsubscribeButtonSettings['profile_id'] = $profile_author_id;
		$SubscribeUnsubscribeButtonSettings['title'] = 'Subscribe';
		$SubscribeUnsubscribeButtonSettings['url'] = array(
			'controller' => 'profiles',
			'action' => 'subscribe',
			$profile_author_id
		);	
		if($this->Subscription->existsSubscription($profile_author_id, $profile_reader_id)) {
			$SubscribeUnsubscribeButtonSettings['title'] = 'Unsubscribe';
			$SubscribeUnsubscribeButtonSettings['url'] = array(
				'controller' => 'profiles',
				'action' => 'unsubscribe',
				$profile_author_id
			);
		}
		return $SubscribeUnsubscribeButtonSettings;
	}

	public function getAuthorizatedProfileId() {
		return $this->Profile->getProfileIdByUserId($this->Auth->user('id'));
	}

	public function getSubscribersCount($profile_id) {
		return $this->Subscription->getSubscribersCount($profile_id);
	}

	public function getSubscriptionsCount($profile_id) {
		return $this->Subscription->getSubscriptionsCount($profile_id);
	}


	public function subscribe($profile_author_id = null) {
		if(!$this->Profile->exists($profile_author_id)) {
			throw new NotFoundException(__('An unsupported operation!'));
			return;
		}
		if(!$this->Auth->user()) {
			return $this->redirect($this->Auth->redirectUrl());
		}		
		$profile_reader_id = $this->Profile->getProfileIdByUserId($this->Auth->user('id'));
		if ($profile_reader_id == $profile_author_id) {
			throw new NotFoundException(__('An unsupported operation!'));
			return;
		}
		$response['button_title'] = 'Unsubscribe';
		$response['message'] = 'You have been successfully subscribed!';
		$response['status'] = true;
		if(!$this->Subscription->existsSubscription($profile_author_id, $profile_reader_id)) {
			//To subscribe (add subscription)
			if(!$this->Subscription->addSubscription($profile_author_id, $profile_reader_id)) {
				$response['status'] = false;
				$response['message'] = 'The subscription could not be added. Please, try again';
			}
		}
		if ($response['status']){
			$response['subscribers_count'] = $this->Subscription->getSubscribersCount($profile_author_id);
		}

		if($this->RequestHandler->isAjax()) {
			$this->set(compact('response'));
			$this->set('_serialize', 'response');
		} else {
			$this->Session->setFlash($response['message']);
			return $this->redirect($this->referer());
		}
	}

	public function unsubscribe($profile_author_id = null) {
		if(!$this->Profile->exists($profile_author_id)) {
			throw new NotFoundException(__('An unsupported operation!'));
			return;
		}
		if(!$this->Auth->user()) {
			return $this->redirect($this->Auth->redirectUrl());
		}		
		$profile_reader_id = $this->Profile->getProfileIdByUserId($this->Auth->user('id'));
		if ($profile_reader_id == $profile_author_id) {
			throw new NotFoundException(__('An unsupported operation!'));
			return;
		}
		$response['button_title'] = 'Subscribe';
		$response['message'] = 'You have been successfully unsubscribed!';
		$response['status'] = true;
		if($this->Subscription->existsSubscription($profile_author_id, $profile_reader_id)) {
			//To subscribe (add subscription)
			if(!$this->Subscription->deleteSubscription($profile_author_id, $profile_reader_id)) {
				$response['status'] = false;
				$response['message'] = 'The subscription could not be deleted. Please, try again';
			}
		}
		if ($response['status']){
			$response['subscribers_count'] = $this->Subscription->getSubscribersCount($profile_author_id);
		}
		if($this->RequestHandler->isAjax()) {
			$this->set(compact('response'));
			$this->set('_serialize', 'response');
		} else {
			$this->Session->setFlash($response['message']);
			return $this->redirect($this->referer());
		}
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {

			$this->request->data['Profile']['user_id'] = $this->Auth->user('id');

			if ($this->Profile->save($this->request->data)) {
				$this->Session->setFlash(__('The profile has been saved.'));
				return $this->redirect(array('action' => 'add'));
			} else {
				$this->Session->setFlash(__('The profile could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */

	
	public function edit() {
		if ($this->request->is(array('post', 'put'))) {
			$options = array(
				'conditions' => array('User.' . $this->User->primaryKey => $this->Auth->user('id')
			));
			if($this->Profile->find('first', $options)) {
				$profile = $this->Profile->find('first', $options);
				$this->Profile->id = $profile['Profile']['id'];
				$this->set('profile', $this->Profile->find('first', $options));	
			} else {
				$this->Profile->create();
			}

			$this->request->data['Profile']['user_id'] = $this->Auth->user('id');

			if ($this->Profile->save($this->request->data)) {
				$this->Session->setFlash(__('The profile has been saved.'));
				return $this->redirect($this->referer());
			} else {
				$this->Session->setFlash(__('The profile could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('User.' . $this->User->primaryKey => $this->Auth->user('id')));
			$this->request->data = $this->Profile->find('first', $options);
			$this->set('profile', $this->Profile->find('first', $options));		
		}
	}
	
/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Profile->id = $id;
		if (!$this->Profile->exists()) {
			throw new NotFoundException(__('Invalid profile'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Profile->delete()) {
			$this->Session->setFlash(__('The profile has been deleted.'));
		} else {
			$this->Session->setFlash(__('The profile could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Profile->recursive = 0;
		$this->set('profiles', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Profile->exists($id)) {
			throw new NotFoundException(__('Invalid profile'));
		}
		$options = array('conditions' => array('Profile.' . $this->Profile->primaryKey => $id));
		$this->set('profile', $this->Profile->find('first', $options));		
	}


/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Profile->create();
			if ($this->Profile->save($this->request->data)) {
				$this->Session->setFlash(__('The profile has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The profile could not be saved. Please, try again.'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Profile->exists($id)) {
			throw new NotFoundException(__('Invalid profile'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Profile->save($this->request->data)) {
				$this->Session->setFlash(__('The profile has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The profile could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Profile.' . $this->Profile->primaryKey => $id));
			$this->request->data = $this->Profile->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Profile->id = $id;
		if (!$this->Profile->exists()) {
			throw new NotFoundException(__('Invalid profile'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Profile->delete()) {
			$this->Session->setFlash(__('The profile has been deleted.'));
		} else {
			$this->Session->setFlash(__('The profile could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	public function like($postId = null) {
		$this->layout = 'ajax';
		$this->autoRender = false;
		$userId = $this->Auth->user('id');
		$profileId = $this->Profile->getProfileIdByUserId($userId);
		if ($this->Like->checkLike($postId, $profileId)) {
			$this->Like->deleteLike($postId, $profileId);
		} else {
			$this->Like->addLike($postId, $profileId);
		}
		$count = $this->Like->countLike($postId);
		return $count;
		//return $this->redirect($this->referer());
	}

	public function visitEvent($eventId = null, $res = null) {
		$userId = $this->Auth->user('id');
		$profileId = $this->Profile->getProfileIdByUserId($userId);
		$profile = $this->Profile->find('first', array('conditions' => array('Profile.id' => $profileId)));
		foreach ($profile['Visiting'] as $prof) {
			$res[] = $prof['id'];
		}
		$res[] += $eventId;
		$this->Profile->save(array(
			'Profile' => array(
				'id' => $profileId
				),
			'Visiting' => $res
			)
		);
		return $this->redirect($this->referer());
	}

	public function notVisitEvent($eventId = null, $edit = null) {
		$userId = $this->Auth->user('id');
		$profileId = $this->Profile->getProfileIdByUserId($userId);
		$profile = $this->Profile->find('first', array('conditions' => array('Profile.id' => $profileId)));
		foreach ($profile['Visiting'] as $prof) {			
			if($prof['id'] == $eventId) {
				unset($prof);
			} else {
				$edit[] = $prof;
			}
		}		
		if (empty($edit)) {
			$profile['Visiting'] = array();
		} else {
			$profile['Visiting'] = $edit;
		}
		//pr($profile);
		$this->Profile->save($profile);		
		return $this->redirect($this->referer());
	}

}


