<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');
App::uses('CakeEmail', 'Network/Email');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
	public $components = array(
        'Session',
        'Auth' => array(
            'loginRedirect' => array(
                'controller' => 'profiles',
                'action' => 'edit'
            ),
            'logoutRedirect' => array(
                'controller' => 'pages',
                'action' => 'main',
            ),
            'authenticate' => array(
                'Form' => array(
                    'passwordHasher' => 'Blowfish',
                    'fields' => array('username' => 'email')
                )
            ),
        )
    );

    public function beforeFilter() {
        
        if($this->request->is('ajax')){
            $this->layout = 'ajax';
        }
        $this->Auth->allow('index', 'view', 'display', 'main');

        $this->helpers['Html'] = array('className' => 'BoostCake.BoostCakeHtml');
		$this->helpers['Form'] = array('className' => 'BoostCake.BoostCakeForm');
		$this->helpers['Paginator'] = array('className' => 'BoostCake.BoostCakePaginator');
    }

    public function sendEmail($from, $to, $subject, $view, $layout, $data = null) {
        $email = new CakeEmail('mandrill');
        //$email = new CakeEmail();
        $email->template($view, $layout);
        $email->emailFormat('both');
        $email->viewVars($data);
        $email->to($to);
        $email->from($from);
        $email->subject($subject);
        $email->send();
    }
}