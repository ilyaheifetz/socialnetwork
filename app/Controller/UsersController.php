<?php
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class UsersController extends AppController {

/**
 * Components
 *
 * @var array
 */	
	public $components = array('Paginator', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->User->recursive = 0;
		$this->set('users', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
		$this->set('user', $this->User->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		//pr($this->User->generateEmailToken());
		if ($this->request->is('post')) {
			$this->request->data['User']['emailtoken'] = hash('md5', uniqid());
			$adminEmail = Configure::read('Admin.email');
			$this->User->create();
			if ($this->User->save($this->request->data)) {
				//$emailToken = $this->User->generateEmailToken();			
				$this->sendEmail(
					$adminEmail,					
					$this->request->data['User']['email'],
					'Confirm your e-mail',
					'after_registration',
					'default',
					array('user' => $this->request->data)					
				);
				$this->Session->setFlash(__('Please check your email and confirm your email address to complete the registration process!'),
					'default',
					array(),
				'registered');
				//http://www.blogtyrant.com/important-please-activate-your-subscription/
				//$this->Session->setFlash(__('Please activate your account via e-mail.'));
				// Переадресация на страницу с сообщением о необходимости подтвердить e-mail
				//return $this->redirect(array('action' => 'login'));
				return $this->redirect($this->referer());
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'));
			}
		}
	}

	public function login() {
	    if ($this->request->is('post')) {
			if ($this->Auth->login()) {
				$user = $this->User->findById($this->Auth->user('id'));
				if($user['User']['status'] != 1) {
					$this->Session->setFlash(__('Your account has not yet been confirmed. Please check your email.'));
					$this->Auth->logout();
					return $this->redirect($this->referer());
				}
		        return $this->redirect(array('controller' => 'profiles', 'action' => 'edit'));
		    }
	        $this->Session->setFlash(__('Invalid e-mail or password, try again'));
	    }
	}
	//your account has not yet been confirmed please check your email
	public function logout() {
	    return $this->redirect($this->Auth->logout());
	}

	public function beforeFilter() {
	    parent::beforeFilter();
	    // Allow users to register and logout.
	    $this->Auth->allow('add', 'logout', 'confirm');
	}


/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->User->save($this->request->data)) {
				//$this->Session->setFlash(__('The user has been saved.'));
				$this->Session->setFlash(__('The user has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
			$this->request->data = $this->User->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->User->delete()) {
			$this->Session->setFlash(__('The user has been deleted.'));
		} else {
			$this->Session->setFlash(__('The user could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->User->recursive = 0;
		$this->set('users', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
		$this->set('user', $this->User->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->User->create();
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('The user has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('The user has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
			$this->request->data = $this->User->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->User->delete()) {
			$this->Session->setFlash(__('The user has been deleted.'));
		} else {
			$this->Session->setFlash(__('The user could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	
	public function confirm($token = null) {
		if($token && $this->User->findByEmailtoken($token)) {
			//Ищем первого по $token
			$user = $this->User->findByEmailtoken($token);
			$this->User->id = $user['User']['id'];
			$this->User->set('emailtoken', null);
			$this->User->set('status', 1);
			$adminEmail = Configure::read('Admin.email');
			if($this->User->save()) {
				//$this->Session->setFlash(__('Your account is activated.'));				
				$this->sendEmail(
					$adminEmail,
					$user['User']['email'],
					'Your account has now been confirmed',
					'after_email_confirmation',
					'default'				
				);				
				$this->Session->setFlash(__('Congratulations! Your e-mail has now been confirmed. You can now log in to your account.'),
					'default',
					array(),
				'confirmed');				
				return $this->redirect(array('action' => 'add'));
			}		
		}
		return $this->redirect(array('controller' => 'pages', 'action' => 'main'));
	}
}
