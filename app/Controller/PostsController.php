<?php
App::uses('AppController', 'Controller');
/**
 * Posts Controller
 *
 * @property Post $Post
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class PostsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $uses = array('Post', 'Profile', 'User', 'Like', 'Subscription');
	public $components = array('Paginator', 'Session');

	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('getCommentsCountByProfileId');
	}

/**
 * index method
 *
 * @return void
 */
/*
	public function index() {
		$this->Post->recursive = 0;
		$this->set('posts', $this->Paginator->paginate());
	}
*/
/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Post->exists($id)) {
			//throw new NotFoundException(__('Invalid post'));
			return $this->redirect('/');
		}
		/*
		$options = array('conditions' => array('Post.' . $this->Post->primaryKey => $id));
		$this->set('post', $this->Post->find('first', $options));
		*/

		//$post = $this->Post->findById($id);
		$post = $this->Post->getPostWithComments($id);
		$this->set('post', $post);

		if ($this->Auth->user() && $this->request->is('post')) {
			$profile_id = $this->Profile->getProfileIdByUserId($this->Auth->user('id'));
			$this->request->data['Post']['profile_id'] = $profile_id;
			$this->request->data['Post']['title'] = 'Re: ' . $post['Post']['title'];
			$this->request->data['Post']['parent_id'] = $post['Post']['id'];

			$this->Post->create();
			if ($this->Post->save($this->request->data)) {
				$lastInsertId = $this->Post->getLastInsertId();
				$this->Session->setFlash(__('The comment has been saved.'));
				return $this->redirect('#comment_id=' . $lastInsertId);
			} else {
				$this->Session->setFlash(__('The comment could not be saved. Please, try again.'));
			}
		}
		$userId = $this->Auth->user('id');
		$profileId = $this->Profile->getProfileIdByUserId($userId);
		$likingPosts = $this->Like->likingPosts($profileId);
		$allLikesCurrentProfiles = array();
		foreach ($likingPosts as $like) {
            $allLikesCurrentProfiles[] = $like['Like']['post_id'];
        }
        $this->set(compact('userId', 'allLikesCurrentProfiles', 'profileId'));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {

			$options = array(
				'conditions' => array('User.' . $this->User->primaryKey => $this->Auth->user('id')
			));
			$profile = $this->Profile->find('first', $options);
			$this->request->data['Post']['profile_id'] = $profile['Profile']['id'];
        	//pr($this->request->data);

			$this->Post->create();
			if ($this->Post->save($this->request->data)) {
				$this->Session->setFlash(__('The post has been saved.'));
				//return $this->redirect(array('action' => 'index'));
				return $this->redirect('/');
			} else {
				$this->Session->setFlash(__('The post could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		//pr($this->referer());
		if (!$this->Post->exists($id)) {
			throw new NotFoundException(__('Invalid post'));
		}
		if($this->Post->getPostProfileIdByPostId($id) != $this->Profile->getProfileIdByUserId($this->Auth->user('id'))) {
			return $this->redirect('/');
		}
		$this->Post->id = $id;
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Post->save($this->request->data)) {				
				if($postParentId = $this->Post->getPostParentIdByPostId($id)) {
					$this->Session->setFlash(__('The comment has been saved.'));
					return $this->redirect(array(
						'controller' => 'posts',
						'action' => 'view',
						$postParentId,
						'#' => "comment_id=$id"
					));
				}
				$this->Session->setFlash(__('The post has been saved.'));
				return $this->redirect(array(
					'controller' => 'pages',
					'action' => 'main',
					'#' => "post_id=$id"
				));
			} else {
				$this->Session->setFlash(__('The post could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Post.' . $this->Post->primaryKey => $id));
			$post = $this->Post->find('first', $options);
			$this->request->data = $post;
			$this->set(compact('post'));
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */

	public function delete($id = null) {
		//pr($this->request);
		//pr($_SERVER);		
		$this->Post->id = $id;
		if (!$this->Post->exists()) {
			throw new NotFoundException(__('Invalid post'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Post->delete()) {
			$this->Session->setFlash(__('The post has been deleted.'));
		} else {
			$this->Session->setFlash(__('The post could not be deleted. Please, try again.'));
		}
		//return $this->redirect(array('action' => 'index'));		
		return $this->redirect($this->referer());
	}

/**
 * admin_index method
 *
 * @return void
 */

/*
	public function admin_index() {
		$this->Post->recursive = 0;
		$this->set('posts', $this->Paginator->paginate());
	}
*/
/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
/*
	public function admin_view($id = null) {
		if (!$this->Post->exists($id)) {
			throw new NotFoundException(__('Invalid post'));
		}
		$options = array('conditions' => array('Post.' . $this->Post->primaryKey => $id));
		$this->set('post', $this->Post->find('first', $options));
	}
*/
/**
 * admin_add method
 *
 * @return void
 */
/*
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Post->create();
			if ($this->Post->save($this->request->data)) {
				$this->Session->setFlash(__('The post has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The post could not be saved. Please, try again.'));
			}
		}
		$profiles = $this->Post->Profile->find('list');
		$this->set(compact('profiles'));
	}
*/
/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
/*
	public function admin_edit($id = null) {
		if (!$this->Post->exists($id)) {
			throw new NotFoundException(__('Invalid post'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Post->save($this->request->data)) {
				$this->Session->setFlash(__('The post has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The post could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Post.' . $this->Post->primaryKey => $id));
			$this->request->data = $this->Post->find('first', $options);
		}
		$profiles = $this->Post->Profile->find('list');
		$this->set(compact('profiles'));
	}
*/
/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
/*
	public function admin_delete($id = null) {
		$this->Post->id = $id;
		if (!$this->Post->exists()) {
			throw new NotFoundException(__('Invalid post'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Post->delete()) {
			$this->Session->setFlash(__('The post has been deleted.'));
		} else {
			$this->Session->setFlash(__('The post could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
*/
	public function like_posts() {
		$userId = $this->Auth->user('id');
		$profileId = $this->Profile->getProfileIdByUserId($userId);
		$likingPost = $this->Like->likingPosts($profileId);
		$this->set(compact('likingPost'));
	}

	public function subscription_posts() {
		$userId = $this->Auth->user('id');
		$profileId = $this->Profile->getProfileIdByUserId($userId);
		$subscriptionProfileAuthorIdsArray =
			$this->Subscription->getSubscriptionProfileAuthorIdsArrayByProfileReaderId($profileId);
		$subscriptionPosts = $this->Post->getSubscriptionPostsByProfileIdsArray($subscriptionProfileAuthorIdsArray);
		$likingPosts = $this->Like->likingPosts($profileId);
		if(!$likingPosts) {
			$likingPosts = array();
		}
		foreach($likingPosts as $like) {
            $allLikesCurrentProfiles[] = $like['Like']['post_id'];
        }
        if(!isset($allLikesCurrentProfiles)) {
			$allLikesCurrentProfiles = array();
		}
        $this->set(array(
        	'posts' => $subscriptionPosts,
        	'userId' => $userId,
        	'profileId' => $profileId,
      		'allLikesCurrentProfiles' => $allLikesCurrentProfiles
        ));      
	}
}
