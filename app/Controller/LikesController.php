<?php
App::uses('AppController', 'Controller');
/**
 * Posts Controller
 *
 * @property Post $Post
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class LikesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $uses = array('Post', 'Profile', 'User');
	public $components = array('Paginator', 'Session');

/**
 * index method
 *
 * @return void
 */
	
}
