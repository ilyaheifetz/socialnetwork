<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
	Router::connect('/', array('controller' => 'pages', 'action' => 'main'));
	Router::connect('/registration/confirm/*', array('controller' => 'users', 'action' => 'confirm'));
	Router::connect('/registration', array('controller' => 'users', 'action' => 'add'));
	Router::connect('/login', array('controller' => 'users', 'action' => 'login'));
	Router::connect('/logout', array('controller' => 'users', 'action' => 'logout'));
	Router::connect('/new_post', array('controller' => 'posts', 'action' => 'add'));
	Router::connect('/profile', array('controller' => 'profiles', 'action' => 'edit'));	
	Router::connect('/subscription_posts', array('controller' => 'posts', 'action' => 'subscription_posts'));
	Router::connect('/subscribers', array('controller' => 'subscriptions', 'action' => 'subscribers'));
	Router::connect('/my_events', array('controller' => 'events', 'action' => 'index'));
	Router::connect('/events', array('controller' => 'events', 'action' => 'all_events'));
	Router::connect('/new_event', array('controller' => 'events', 'action' => 'add'));
	

	Router::parseExtensions('json');

/**
 * ...and connect the rest of 'Pages' controller's URLs.
 */
	Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));

/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';


