<?php

use Phinx\Migration\AbstractMigration;

class PostTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->query("CREATE TABLE `posts` (
            `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
            `profile_id` INT(10) UNSIGNED NOT NULL DEFAULT '0',
            `title` VARCHAR(50) NOT NULL,
            `body` TEXT NOT NULL,
            `created` DATETIME NULL DEFAULT NULL,
            `modified` DATETIME NULL DEFAULT NULL,
            PRIMARY KEY (`id`))"
        );
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->query("DROP TABLE `posts`;");
    }
}