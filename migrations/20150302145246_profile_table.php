<?php

use Phinx\Migration\AbstractMigration;

class ProfileTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->query("CREATE TABLE `profiles` (
            `id` INT(11) NOT NULL AUTO_INCREMENT,
            `firstname` VARCHAR(250) NULL DEFAULT NULL,
            `lastname` VARCHAR(250) NULL DEFAULT NULL,
            `username` VARCHAR(250) NULL DEFAULT NULL,
            `userpic` VARCHAR(250) NULL DEFAULT NULL,
            `age` DATE NULL DEFAULT NULL,
            `bio` TEXT NULL,
            `active` TINYINT(4) NULL DEFAULT '0',
            `user_id` INT(11) NOT NULL DEFAULT '0',
            `created` DATETIME NULL DEFAULT NULL,
            `updated` DATETIME DEFAULT NULL,
            PRIMARY KEY (`id`)
        )
        COLLATE='utf8_general_ci'
        ENGINE=InnoDB
        ;");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->query("DROP  TABLE IF EXISTS `profiles`");
    }
}