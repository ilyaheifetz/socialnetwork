<?php

use Phinx\Migration\AbstractMigration;

class UserTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->query("CREATE TABLE `users` (
            `id` INT(11) NOT NULL AUTO_INCREMENT,
            `email` VARCHAR(250) NOT NULL DEFAULT '0',
            `password` VARCHAR(250) NOT NULL DEFAULT '0',            
            `created` DATETIME NULL DEFAULT NULL,
            `modified` DATETIME NULL DEFAULT NULL,
            PRIMARY KEY (`id`)
        )
        COLLATE='utf8_general_ci'
        ENGINE=InnoDB
        ;");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->query("DROP  TABLE IF EXISTS `users`");
    }
}