<?php

use Phinx\Migration\AbstractMigration;

class LikeTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->query("CREATE TABLE `likes` (
            `id` INT(10) NOT NULL AUTO_INCREMENT,
            `profile_id` INT(10) NOT NULL,
            `post_id` INT(10) NOT NULL,
            PRIMARY KEY (`id`)
        )
        COLLATE='utf8_general_ci'
        ENGINE=InnoDB
        ");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->query("DROP  TABLE IF EXISTS `likes`");
    }
}