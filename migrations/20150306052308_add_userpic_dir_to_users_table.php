<?php

use Phinx\Migration\AbstractMigration;

class AddUserpicDirToUsersTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->query("ALTER TABLE `profiles`
            ADD COLUMN `userpic_dir` VARCHAR(250) NULL DEFAULT NULL AFTER `userpic`;"
        );
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->query("ALTER TABLE `profiles`
            DROP COLUMN `userpic_dir`;"
        );
    }
}