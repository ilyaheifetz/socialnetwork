<?php

use Phinx\Migration\AbstractMigration;

class EventsProfilesTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->query("CREATE TABLE `events_profiles` (
            `id` INT(11) NOT NULL AUTO_INCREMENT,
            `profile_id` INT(11) NOT NULL,
            `event_id` INT(11) NOT NULL,
            PRIMARY KEY (`id`)
        )
        ENGINE=InnoDB
        ;
        ");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->query("DROP  TABLE IF EXISTS `events_profiles`");
    }
}