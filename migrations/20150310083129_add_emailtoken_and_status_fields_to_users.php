<?php

use Phinx\Migration\AbstractMigration;

class AddEmailtokenAndStatusFieldsToUsers extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->query("ALTER TABLE `users`
            ADD COLUMN `emailtoken` VARCHAR(250) NULL DEFAULT NULL AFTER `password`,
            ADD COLUMN `status` TINYINT NULL DEFAULT '0' AFTER `emailtoken`;
            SELECT `DEFAULT_COLLATION_NAME` FROM `information_schema`.`SCHEMATA` WHERE `SCHEMA_NAME`='socialnetwork';"
        );
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->query("ALTER TABLE `users`
            DROP COLUMN `emailtoken`,
            DROP COLUMN `status`;
            SELECT `DEFAULT_COLLATION_NAME` FROM `information_schema`.`SCHEMATA` WHERE `SCHEMA_NAME`='socialnetwork';"
        );
    }
}