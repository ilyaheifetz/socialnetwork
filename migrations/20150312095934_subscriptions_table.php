<?php

use Phinx\Migration\AbstractMigration;

class SubscriptionsTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->query("CREATE TABLE `subscriptions` (
            `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
            `profile_reader_id` INT(11) UNSIGNED NOT NULL,
            `profile_author_id` INT(11) UNSIGNED NOT NULL,            
            PRIMARY KEY (`id`)
        )");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->query("DROP TABLE `subscriptions`;");
    }
}